package com.jkt.fnolprocess.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jkt.fnolprocess.InsuranceClaimEngineApplicationTest;
import com.jkt.fnolprocess.domains.Claims;
import com.jkt.fnolprocess.util.ResponseDTO;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
class ClaimControllerTest extends InsuranceClaimEngineApplicationTest {
    @Autowired
    ClaimController controller;

    @Test
    void saveClaimGLTest() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        Claims claims = mapper.readValue(gl_payload(), Claims.class);

        ResponseDTO responseDTO = controller.saveClaim(claims);
        Assert.assertEquals(true,responseDTO.getMessages().size()>0);
        Assert.assertEquals("GL",responseDTO.getClaimType());
        Assert.assertEquals(null,responseDTO.getClaimId());
        Assert.assertEquals(false,responseDTO.getIncidentId().isEmpty());


    }
    @Test
    void saveClaimWCTest() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        Claims claims = mapper.readValue(wc_payload(), Claims.class);

        ResponseDTO responseDTO = controller.saveClaim(claims);
        Assert.assertEquals(true,responseDTO.getMessages().size()>0);
        Assert.assertEquals("WC",responseDTO.getClaimType());
        Assert.assertEquals(null,responseDTO.getClaimId());
        Assert.assertEquals(false,responseDTO.getIncidentId().isEmpty());


    }
    @Test
    void saveClaimWCTestEmail() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        Claims claims = mapper.readValue(wc_payloadEmail(), Claims.class);

        ResponseDTO responseDTO = controller.saveClaim(claims);
        Assert.assertEquals(true,responseDTO.getMessages().size()>0);
        Assert.assertEquals("WC",responseDTO.getClaimType());
        Assert.assertEquals(null,responseDTO.getClaimId());
        Assert.assertEquals(false,responseDTO.getIncidentId().isEmpty());


    }
    @Test
    void saveClaimWCTestChatBot() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        Claims claims = mapper.readValue(wc_payloadChatbot(), Claims.class);

        ResponseDTO responseDTO = controller.saveClaim(claims);
        Assert.assertEquals(true,responseDTO.getMessages().size()>0);
        Assert.assertEquals("WC",responseDTO.getClaimType());
        Assert.assertEquals(null,responseDTO.getClaimId());
        Assert.assertEquals(false,responseDTO.getIncidentId().isEmpty());


    }

    @Test
    void getAllClaim() {
        List<Claims>claimsList=controller.getAllClaim();
        Assert.assertEquals(true,claimsList.size()>0);
    }

    @Test
    void getWCClaim() {
        Claims claimsWC=controller.getClaim("I1630486917860");
        Assert.assertEquals("I1630486917860",claimsWC.getIncidentDetails().getIncidentId());
        Assert.assertEquals(true,claimsWC.getIncidentDetails().getClaimsLists().get(0).getClaimDetails().getClaimType()==2);
    }
    @Test
    void getGLClaim(){
        Claims claimsGL=controller.getClaim("I1630487658091");
        Assert.assertEquals("I1630487658091",claimsGL.getIncidentDetails().getIncidentId());
        Assert.assertEquals(true,claimsGL.getIncidentDetails().getClaimsLists().get(0).getClaimDetails().getClaimType()==1);
    }

    @Test
    void getClaimbyEmployee() {
        List<Claims> claimsList=controller.getClaimbyEmployee(1);
        Assert.assertEquals(true,claimsList.size()>=0);
    }

    @Test
    void getClaimbyEmployer() {
        List<Claims> claimsList=controller.getClaimbyEmployer(1);
        Assert.assertEquals(true,claimsList.size()>=0);
    }

    @Test
    void getClaimbyInsured() {
        List<Claims> claimsList=controller.getClaimbyInsured(1);
        Assert.assertEquals(true,claimsList.size()>=0);
    }
    @Test
    void updateWCClaim() throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        Claims claims = mapper.readValue(wcUpdatePayload(), Claims.class);

        ResponseDTO responseDTO = controller.updateClaim(claims);
        Assert.assertEquals(true,responseDTO.getMessages().size()>0);
        Assert.assertEquals(null,responseDTO.getClaimId());
        Assert.assertEquals(false,responseDTO.getIncidentId().isEmpty());

    }
    @Test
    void updateGLClaim() throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        Claims claims = mapper.readValue(glUpdatePayload(), Claims.class);

        ResponseDTO responseDTO = controller.updateClaim(claims);
        Assert.assertEquals(true,responseDTO.getMessages().size()>0);
        Assert.assertEquals(null,responseDTO.getClaimId());
        Assert.assertEquals(false,responseDTO.getIncidentId().isEmpty());

    }


    private String gl_payload() {
        String json = "{\n" +
                "    \"incidentDetails\": {\n" +
                "            \"subClaimType\": 4,\n" +
                "\t\"claimsLists\":[\n" +
                "        {\n" +
                "        \"claimDetails\": {\n" +
                "            \"claimType\": \"1\",\n" +
                "            \"claimStatus\": \"1\",\n" +
                "\t\t\t\"subClaimType\":4,\n" +
                "            \"claimSourceType\": \"1\"\n" +
                "        },\n" +
                "        \"documentDetails\": [{\n" +
                "            \"documentTitle\": \"Claim Form\",\n" +
                "            \"documentURL\": \"https://picsum.photos/200/300\",\n" +
                "            \"documentType\": \"1\",\n" +
                "            \"documentFormat\": \"png\"\n" +
                "        }],\n" +
                "        \"glOccuranceDetails\": {\n" +
                "            \"policeContacted\": \"No\",\n" +
                "            \"reportNumber\": \"\",\n" +
                "            \"occurrenceDescription\": \"\",        \n" +
                "            \"address\": {\n" +
                "                \"addressName\": \"1108  Warner Street\",\n" +
                "                \"addressCity\": \"Miami\",\n" +
                "                \"addressState\": 1,\n" +
                "                \"addressCountry\": 1,\n" +
                "                \"addressZip\": \"33139\"\n" +
                "            }\n" +
                "        },\n" +
                "        \"glLiabilityDetails\": {\n" +
                "            \"premisesInsured\": \"Owner\",\n" +
                "            \"productInsured\": \"None\",\n" +
                "            \"typeOfPremises\": \"\"\n" +
                "        },\n" +
                "        \"glInjuredDetails\": {\n" +
                "            \"firstName\": \"John\",\n" +
                "            \"lastName\": \"Smith\",\n" +
                "            \"primaryPhone\": \"111-111-1111\",\n" +
                "            \"primaryPhoneType\":\"CELL\",\n" +
                "            \"primaryEmail\": \"john@xyz.com\",\n" +
                "            \"injuryCode\":18,\n" +
                "            \"injuryDescription\":\"\",   \n" +
                "\t\t\t\"whereTaken\":\"\",\n" +
                "\t\t\t\"initialTreatment\":\"EMERGENCY CARE\",\n" +
                "            \"dob\": \"2016-08-29\",\n" +
                "            \"age\": 25,\n" +
                "            \"sex\": \"Male\",\n" +
                "            \"address\": {\n" +
                "                \"addressName\": \"1108  Warner Street\",\n" +
                "                \"addressCity\": \"Miami\",\n" +
                "                \"addressState\": 1,\n" +
                "                \"addressCountry\": 1,\n" +
                "                \"addressZip\": \"33139\"\n" +
                "            }\n" +
                "        },\n" +
                "        \"glClaimantDetails\": {\n" +
                "        \"primaryEmail\": \"macd@jktech.com\"\n" +
                "        },      \n" +
                "        \"witnessDetails\": [\n" +
                "            {\n" +
                "            \"witnessName\": \"Josephin\",\n" +
                "            \"phone\": \"810-292-9388\",\n" +
                "            \"email\": \"josephine_darakjy@jktech.com\",\n" +
                "            \"address\": {\n" +
                "                \"addressName\": \"1108  Warner Street\",\n" +
                "                \"addressCity\": \"Miami\",\n" +
                "                \"addressState\": 1,\n" +
                "                \"addressCountry\": 1,\n" +
                "                \"addressZip\": \"33139\"\n" +
                "            }\n" +
                "        }]\n" +
                "    },\n" +
                "\t\n" +
                "\t{\n" +
                "        \"claimDetails\": {\n" +
                "            \"claimType\": \"1\",\n" +
                "            \"claimStatus\": \"1\",\n" +
                "            \"claimSourceType\": \"1\",\n" +
                "\t\t\t\"subClaimType\":5\n" +
                "        },\n" +
                "        \"documentDetails\": [{\n" +
                "            \"documentTitle\": \"Claim Form\",\n" +
                "            \"documentURL\": \"https://picsum.photos/200/300\",\n" +
                "            \"documentType\": \"1\",\n" +
                "            \"documentFormat\": \"png\"\n" +
                "        }],\n" +
                "        \"glOccuranceDetails\": {\n" +
                "            \"policeContacted\": \"No\",\n" +
                "            \"reportNumber\": \"\",\n" +
                "            \"occurrenceDescription\": \"\",        \n" +
                "            \"address\": {\n" +
                "                \"addressName\": \"1108  Warner Street\",\n" +
                "                \"addressCity\": \"Miami\",\n" +
                "                \"addressState\": 1,\n" +
                "                \"addressCountry\": 1,\n" +
                "                \"addressZip\": \"33139\"\n" +
                "            }\n" +
                "        },\n" +
                "        \"glLiabilityDetails\": {\n" +
                "            \"premisesInsured\": \"Owner\",\n" +
                "            \"productInsured\": \"None\",\n" +
                "            \"typeOfPremises\": \"\"\n" +
                "        },\n" +
                "        \"glClaimantDetails\": {\n" +
                "        \"primaryEmail\": \"macd@jktech.com\"\n" +
                "        },\n" +
                "        \"propertyDetails\": {\n" +
                "            \"firstName\": \"John\",\n" +
                "            \"lastName\": \"Smith\",\n" +
                "\t\t\t \"dob\": \"2016-08-29\",\n" +
                "            \"age\": 25,\n" +
                "            \"sex\": \"Male\",\n" +
                "            \"primaryPhone\": \"111-111-1111\",\n" +
                "            \"primaryPhoneType\":\"CELL\",\n" +
                "            \"primaryEmail\": \"jbutt@jktech.com\",\n" +
                "            \"describeProperty\": \"\",\n" +
                "            \"estimateAmount\": 100,\n" +
                "            \"wherePropertyCanBeSeen\": \"\",\n" +
                "            \"address\": {\n" +
                "                \"addressName\": \"1108  Warner Street\",\n" +
                "                \"addressCity\": \"Miami\",\n" +
                "                \"addressState\": 1,\n" +
                "                \"addressCountry\": 1,\n" +
                "                \"addressZip\": \"33139\"\n" +
                "            }\n" +
                "        },\n" +
                "        \"witnessDetails\": [\n" +
                "            {\n" +
                "            \"witnessName\": \"Josephin\",\n" +
                "            \"phone\": \"810-292-9388\",\n" +
                "            \"email\": \"josephine_darakjy@jktech.com\",\n" +
                "            \"address\": {\n" +
                "                \"addressName\": \"1108  Warner Street\",\n" +
                "                \"addressCity\": \"Miami\",\n" +
                "                \"addressState\": 1,\n" +
                "                \"addressCountry\": 1,\n" +
                "                \"addressZip\": \"33139\"\n" +
                "            }\n" +
                "        }]\n" +
                "    }\n" +
                "\t]\n" +
                "}\n" +
                "\t\n" +
                "}";
        return json;
    }
    private String wc_payload() {
        String json = "{\n" +
                "\t\"incidentDetails\": {\n" +
                "\t\"subClaimType\": 1,\n" +
                "\t\"claimsLists\":[\n" +
                "\t{\n" +
                "\t\t\"claimDetails\": {\n" +
                "\t\t\t\"claimType\": 2,\n" +
                "\t\t\t\"claimStatus\": \"1\",\n" +
                "\t\t\t\"claimSourceType\": 1,\n" +
                "\t\t\t\t\"subClaimType\": 1\n" +
                "\n" +
                "\t\t},\n" +
                "\t\t\"documentDetails\": [{\n" +
                "\t\t\t\"documentTitle\": \"Claim Form\",\n" +
                "\t\t\t\"documentURL\": \"https://picsum.photos/200/300\",\n" +
                "\t\t\t\"documentType\": 1,\n" +
                "\t\t\t\"documentFormat\": \"png\"\n" +
                "\t\t}],\n" +
                "\t\t\"wcOccuranceDetails\": {\n" +
                "\t\t\t\"occurredDepartment\": \"\",\n" +
                "\t\t\t\"employerNotifiedDate\": \"2016-08-29\",\n" +
                "\t\t\t\"lastWorkDate\": \"2016-08-29\",\n" +
                "\t\t\t\"didOccuredOnEmpPremises\": \"Yes\",\n" +
                "\t\t\t\"equipmentList\": \"\",\n" +
                "\t\t\t\"specificActivity\": \"\",\n" +
                "\t\t\t\"safetyEqupProvided\": \"No\",\n" +
                "\t\t\t\"safetyEqupUsed\": \"No\",\n" +
                "\t\t\t\"occurrenceDetails\": \"\",\n" +
                "\t\t\t\"workProcess\": \"\",\n" +
                "\t\t\t\"eventSequence\": \"\",\n" +
                "\t\t\t\"ocurrenceTime\": \"2016-08-29T09:12:33.001Z\",\n" +
                "\t\t\t\"address\": {\n" +
                "\t\t\t\t\"addressName\": \"1108  Warner Street\",\n" +
                "\t\t\t\t\"addressCity\": \"Miami\",\n" +
                "\t\t\t\t\"addressState\": 1,\n" +
                "\t\t\t\t\"addressCountry\": 1,\n" +
                "\t\t\t\t\"addressZip\": \"33139\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t\"injuryDetails\": [{\n" +
                "\t\t\t\"injuryDate\": \"2016-08-29\",\n" +
                "\t\t\t\"injuryCause\": 1,\n" +
                "\t\t\t\"injuryType\": 1,\n" +
                "\t\t\t\"bodyCode\": 1,\n" +
                "\t\t\t\"fatalStatus\": \"No\",\n" +
                "\t\t\t\"deathDate\": \"2016-08-29\",\n" +
                "\t\t\t\"beginWork\": \"Yes\",\n" +
                "\t\t\t\"returnDate\": \"2016-08-29\",\n" +
                "\t\t\t\"disabilityBeganDate\": \"2016-08-29\",\n" +
                "\t\t\t\"treatmentDetails\": [{\n" +
                "\t\t\t\t\"physicianName\": \"Jame\",\n" +
                "\t\t\t\t\"hospName\": \"\",\n" +
                "\t\t\t\t\"initialTreatment\": \"NO MEDICAL TREATMENT\",\n" +
                "\t\t\t\t\"administratorNotifiedDate\": \"2016-08-29\",\n" +
                "\t\t\t\t\"address\": {\n" +
                "\t\t\t\t\t\"addressName\": \"1108  Warner Street\",\n" +
                "\t\t\t\t\t\"addressCity\": \"Miami\",\n" +
                "\t\t\t\t\t\"addressState\": 1,\n" +
                "\t\t\t\t\t\"addressCountry\": 1,\n" +
                "\t\t\t\t\t\"addressZip\": \"33139\"\n" +
                "\t\t\t\t}\n" +
                "\t\t\t}]\n" +
                "\t\t}],\n" +
                "\t\t\"wcClaimantDetails\": {\n" +
                "\t\t\t\"primaryEmail\": \"jbutt@jktech.com\"\n" +
                "\t\t},\n" +
                "\t\t\"witnessDetails\": [\n" +
                "            {\n" +
                "\t\t\t\"witnessName\": \"Josephin\",\n" +
                "\t\t\t\"phone\": \"810-292-9388\",\n" +
                "\t\t\t\"email\": \"josephine_darakjy@jktech.com\",\n" +
                "\t\t\t\"address\": {\n" +
                "\t\t\t\t\"addressName\": \"1108  Warner Street\",\n" +
                "\t\t\t\t\"addressCity\": \"Miami\",\n" +
                "\t\t\t\t\"addressState\": 1,\n" +
                "\t\t\t\t\"addressCountry\": 1,\n" +
                "\t\t\t\t\"addressZip\": \"33139\"\n" +
                "\t\t\t}\n" +
                "\t\t}]\n" +
                "\t\t}\n" +
                "\t\t]\n" +
                "\t}\n" +
                "}";
        return json;
    }
    private String wc_payloadChatbot() {
        String json = "{\n" +
                "\t\"incidentDetails\": {\n" +
                "\t\"subClaimType\": 1,\n" +
                "\t\"claimsLists\":[\n" +
                "\t{\n" +
                "\t\t\"claimDetails\": {\n" +
                "\t\t\t\"claimType\": 2,\n" +
                "\t\t\t\"claimStatus\": \"1\",\n" +
                "\t\t\t\"claimSourceType\": 3,\n" +
                "\t\t\t\t\"subClaimType\": 1\n" +
                "\n" +
                "\t\t},\n" +
                "\t\t\"documentDetails\": [{\n" +
                "\t\t\t\"documentTitle\": \"Claim Form\",\n" +
                "\t\t\t\"documentURL\": \"https://picsum.photos/200/300\",\n" +
                "\t\t\t\"documentType\": 1,\n" +
                "\t\t\t\"documentFormat\": \"png\"\n" +
                "\t\t}],\n" +
                "\t\t\"wcOccuranceDetails\": {\n" +
                "\t\t\t\"occurredDepartment\": \"\",\n" +
                "\t\t\t\"employerNotifiedDate\": \"2016-08-29\",\n" +
                "\t\t\t\"lastWorkDate\": \"2016-08-29\",\n" +
                "\t\t\t\"didOccuredOnEmpPremises\": \"Yes\",\n" +
                "\t\t\t\"equipmentList\": \"\",\n" +
                "\t\t\t\"specificActivity\": \"\",\n" +
                "\t\t\t\"safetyEqupProvided\": \"No\",\n" +
                "\t\t\t\"safetyEqupUsed\": \"No\",\n" +
                "\t\t\t\"occurrenceDetails\": \"\",\n" +
                "\t\t\t\"workProcess\": \"\",\n" +
                "\t\t\t\"eventSequence\": \"\",\n" +
                "\t\t\t\"ocurrenceTime\": \"2016-08-29T09:12:33.001Z\",\n" +
                "\t\t\t\"address\": {\n" +
                "\t\t\t\t\"addressName\": \"1108  Warner Street\",\n" +
                "\t\t\t\t\"addressCity\": \"Miami\",\n" +
                "\t\t\t\t\"addressState\": 1,\n" +
                "\t\t\t\t\"addressCountry\": 1,\n" +
                "\t\t\t\t\"addressZip\": \"33139\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t\"injuryDetails\": [{\n" +
                "\t\t\t\"injuryDate\": \"2016-08-29\",\n" +
                "\t\t\t\"injuryCause\": 1,\n" +
                "\t\t\t\"injuryType\": 1,\n" +
                "\t\t\t\"bodyCode\": 1,\n" +
                "\t\t\t\"fatalStatus\": \"No\",\n" +
                "\t\t\t\"deathDate\": \"2016-08-29\",\n" +
                "\t\t\t\"beginWork\": \"Yes\",\n" +
                "\t\t\t\"returnDate\": \"2016-08-29\",\n" +
                "\t\t\t\"disabilityBeganDate\": \"2016-08-29\",\n" +
                "\t\t\t\"treatmentDetails\": [{\n" +
                "\t\t\t\t\"physicianName\": \"Jame\",\n" +
                "\t\t\t\t\"hospName\": \"\",\n" +
                "\t\t\t\t\"initialTreatment\": \"NO MEDICAL TREATMENT\",\n" +
                "\t\t\t\t\"administratorNotifiedDate\": \"2016-08-29\",\n" +
                "\t\t\t\t\"address\": {\n" +
                "\t\t\t\t\t\"addressName\": \"1108  Warner Street\",\n" +
                "\t\t\t\t\t\"addressCity\": \"Miami\",\n" +
                "\t\t\t\t\t\"addressState\": 1,\n" +
                "\t\t\t\t\t\"addressCountry\": 1,\n" +
                "\t\t\t\t\t\"addressZip\": \"33139\"\n" +
                "\t\t\t\t}\n" +
                "\t\t\t}]\n" +
                "\t\t}],\n" +
                "\t\t\"wcClaimantDetails\": {\n" +
                "\t\t\t\"primaryEmail\": \"jbutt@jktech.com\"\n" +
                "\t\t},\n" +
                "\t\t\"witnessDetails\": [\n" +
                "            {\n" +
                "\t\t\t\"witnessName\": \"Josephin\",\n" +
                "\t\t\t\"phone\": \"810-292-9388\",\n" +
                "\t\t\t\"email\": \"josephine_darakjy@jktech.com\",\n" +
                "\t\t\t\"address\": {\n" +
                "\t\t\t\t\"addressName\": \"1108  Warner Street\",\n" +
                "\t\t\t\t\"addressCity\": \"Miami\",\n" +
                "\t\t\t\t\"addressState\": 1,\n" +
                "\t\t\t\t\"addressCountry\": 1,\n" +
                "\t\t\t\t\"addressZip\": \"33139\"\n" +
                "\t\t\t}\n" +
                "\t\t}]\n" +
                "\t\t}\n" +
                "\t\t]\n" +
                "\t}\n" +
                "}";
        return json;
    }
    private String wc_payloadEmail() {
        String json = "{\n" +
                "\t\"incidentDetails\": {\n" +
                "\t\"subClaimType\": 1,\n" +
                "\t\"claimsLists\":[\n" +
                "\t{\n" +
                "\t\t\"claimDetails\": {\n" +
                "\t\t\t\"claimType\": 2,\n" +
                "\t\t\t\"claimStatus\": \"1\",\n" +
                "\t\t\t\"claimSourceType\": 2,\n" +
                "\t\t\t\t\"subClaimType\": 1\n" +
                "\n" +
                "\t\t},\n" +
                "\t\t\"documentDetails\": [{\n" +
                "\t\t\t\"documentTitle\": \"Claim Form\",\n" +
                "\t\t\t\"documentURL\": \"https://picsum.photos/200/300\",\n" +
                "\t\t\t\"documentType\": 1,\n" +
                "\t\t\t\"documentFormat\": \"png\"\n" +
                "\t\t}],\n" +
                "\t\t\"wcOccuranceDetails\": {\n" +
                "\t\t\t\"occurredDepartment\": \"\",\n" +
                "\t\t\t\"employerNotifiedDate\": \"2016-08-29\",\n" +
                "\t\t\t\"lastWorkDate\": \"2016-08-29\",\n" +
                "\t\t\t\"didOccuredOnEmpPremises\": \"Yes\",\n" +
                "\t\t\t\"equipmentList\": \"\",\n" +
                "\t\t\t\"specificActivity\": \"\",\n" +
                "\t\t\t\"safetyEqupProvided\": \"No\",\n" +
                "\t\t\t\"safetyEqupUsed\": \"No\",\n" +
                "\t\t\t\"occurrenceDetails\": \"\",\n" +
                "\t\t\t\"workProcess\": \"\",\n" +
                "\t\t\t\"eventSequence\": \"\",\n" +
                "\t\t\t\"ocurrenceTime\": \"2016-08-29T09:12:33.001Z\",\n" +
                "\t\t\t\"address\": {\n" +
                "\t\t\t\t\"addressName\": \"1108  Warner Street\",\n" +
                "\t\t\t\t\"addressCity\": \"Miami\",\n" +
                "\t\t\t\t\"addressState\": 1,\n" +
                "\t\t\t\t\"addressCountry\": 1,\n" +
                "\t\t\t\t\"addressZip\": \"33139\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t\"injuryDetails\": [{\n" +
                "\t\t\t\"injuryDate\": \"2016-08-29\",\n" +
                "\t\t\t\"injuryCause\": 1,\n" +
                "\t\t\t\"injuryType\": 1,\n" +
                "\t\t\t\"bodyCode\": 1,\n" +
                "\t\t\t\"fatalStatus\": \"No\",\n" +
                "\t\t\t\"deathDate\": \"2016-08-29\",\n" +
                "\t\t\t\"beginWork\": \"Yes\",\n" +
                "\t\t\t\"returnDate\": \"2016-08-29\",\n" +
                "\t\t\t\"disabilityBeganDate\": \"2016-08-29\",\n" +
                "\t\t\t\"treatmentDetails\": [{\n" +
                "\t\t\t\t\"physicianName\": \"Jame\",\n" +
                "\t\t\t\t\"hospName\": \"\",\n" +
                "\t\t\t\t\"initialTreatment\": \"NO MEDICAL TREATMENT\",\n" +
                "\t\t\t\t\"administratorNotifiedDate\": \"2016-08-29\",\n" +
                "\t\t\t\t\"address\": {\n" +
                "\t\t\t\t\t\"addressName\": \"1108  Warner Street\",\n" +
                "\t\t\t\t\t\"addressCity\": \"Miami\",\n" +
                "\t\t\t\t\t\"addressState\": 1,\n" +
                "\t\t\t\t\t\"addressCountry\": 1,\n" +
                "\t\t\t\t\t\"addressZip\": \"33139\"\n" +
                "\t\t\t\t}\n" +
                "\t\t\t}]\n" +
                "\t\t}],\n" +
                "\t\t\"wcClaimantDetails\": {\n" +
                "\t\t\t\"primaryEmail\": \"jbutt@jktech.com\"\n" +
                "\t\t},\n" +
                "\t\t\"witnessDetails\": [\n" +
                "            {\n" +
                "\t\t\t\"witnessName\": \"Josephin\",\n" +
                "\t\t\t\"phone\": \"810-292-9388\",\n" +
                "\t\t\t\"email\": \"josephine_darakjy@jktech.com\",\n" +
                "\t\t\t\"address\": {\n" +
                "\t\t\t\t\"addressName\": \"1108  Warner Street\",\n" +
                "\t\t\t\t\"addressCity\": \"Miami\",\n" +
                "\t\t\t\t\"addressState\": 1,\n" +
                "\t\t\t\t\"addressCountry\": 1,\n" +
                "\t\t\t\t\"addressZip\": \"33139\"\n" +
                "\t\t\t}\n" +
                "\t\t}]\n" +
                "\t\t}\n" +
                "\t\t]\n" +
                "\t}\n" +
                "}";
        return json;
    }
    private String wcUpdatePayload(){
        String json="{\n" +
                "\t\"incidentDetails\": {\n" +
                "\t        \"incidentId\": \"I1630486917860\",\n" +
                "\n" +
                "\t\"subClaimType\": 1,\n" +
                "\t\"claimsLists\":[\n" +
                "\t{\n" +
                "\t\t\"claimDetails\": {\n" +
                "\t\t \"claimID\": \"CLID1630486917871\",\n" +
                "                    \"incidentID\": \"I1630486917860\",\n" +
                "\t\t\t\"claimType\": 2,\n" +
                "\t\t\t\"claimStatus\": 1,\n" +
                "\t\t\t\"claimSourceType\": 1,\n" +
                "\t\t\t\t\"subClaimType\": 1\n" +
                "\n" +
                "\t\t},\n" +
                "\t\t\"documentDetails\": [{\n" +
                "\t\t\"documentID\": 154,\n" +
                "                        \"claimID\": \"CLID1630486917871\",\n" +
                "\t\t\t\"documentTitle\": \"Claim Form1\",\n" +
                "\t\t\t\"documentURL\": \"https://picsum.photos/200/3001\",\n" +
                "\t\t\t\"documentType\": 2,\n" +
                "\t\t\t\"documentFormat\": \"jpg\"\n" +
                "\t\t}],\n" +
                "\t\t\"wcOccuranceDetails\": {\n" +
                "\t\t\"occuranceID\": 70,\n" +
                "                    \"claimID\": \"CLID1630486917871\",\n" +
                "\t\t\t\"occurredDepartment\": \"test\",\n" +
                "\t\t\t\"employerNotifiedDate\": \"2016-08-29\",\n" +
                "\t\t\t\"lastWorkDate\": \"2016-08-29\",\n" +
                "\t\t\t\"didOccuredOnEmpPremises\": \"Yes\",\n" +
                "\t\t\t\"equipmentList\": \"\",\n" +
                "\t\t\t\"specificActivity\": \"\",\n" +
                "\t\t\t\"safetyEqupProvided\": \"No\",\n" +
                "\t\t\t\"safetyEqupUsed\": \"No\",\n" +
                "\t\t\t\"occurrenceDetails\": \"test\",\n" +
                "\t\t\t\"workProcess\": \"test\",\n" +
                "\t\t\t\"eventSequence\": \"test\",\n" +
                "\t\t\t\"ocurrenceTime\": \"2016-08-29T09:12:33.001Z\",\n" +
                "\t\t\t\"address\": {\n" +
                "\t\t\t\t\"addressName\": \"1108  Warner Street1\",\n" +
                "\t\t\t\t\"addressCity\": \"Miami\",\n" +
                "\t\t\t\t\"addressState\": 1,\n" +
                "\t\t\t\t\"addressCountry\": 1,\n" +
                "\t\t\t\t\"addressZip\": \"33139\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t\"injuryDetails\": [{\n" +
                "\t\t\t\"injuryID\": 58,\n" +
                "                        \"claimID\": \"CLID1630486917871\",\n" +
                "\t\t\t\"injuryDate\": \"2016-08-29\",\n" +
                "\t\t\t\"injuryCause\": 1,\n" +
                "\t\t\t\"injuryType\": 1,\n" +
                "\t\t\t\"bodyCode\": 2,\n" +
                "\t\t\t\"fatalStatus\": \"Yes\",\n" +
                "\t\t\t\"deathDate\": \"2016-08-29\",\n" +
                "\t\t\t\"beginWork\": \"Yes\",\n" +
                "\t\t\t\"returnDate\": \"2016-08-29\",\n" +
                "\t\t\t\"disabilityBeganDate\": \"2016-08-29\",\n" +
                "\t\t\t\"treatmentDetails\": [{\n" +
                "\t\t\t \"treatmentID\": 59,\n" +
                "                                \"injuryID\": 58,\n" +
                "\t\t\t\t\"physicianName\": \"Jame\",\n" +
                "\t\t\t\t\"hospName\": \"jame\",\n" +
                "\t\t\t\t\"initialTreatment\": \"NO MEDICAL TREATMENT\",\n" +
                "\t\t\t\t\"administratorNotifiedDate\": \"2016-08-29\",\n" +
                "\t\t\t\t\"address\": {\n" +
                "\t\t\t\t\t\"addressName\": \"1108  Warner Street1\",\n" +
                "\t\t\t\t\t\"addressCity\": \"Miami\",\n" +
                "\t\t\t\t\t\"addressState\": 1,\n" +
                "\t\t\t\t\t\"addressCountry\": 1,\n" +
                "\t\t\t\t\t\"addressZip\": \"33139\"\n" +
                "\t\t\t\t}\n" +
                "\t\t\t}]\n" +
                "\t\t}],\n" +
                "\t\t\"wcClaimantDetails\": {\n" +
                "\t\t\"claimantID\": 64,\n" +
                "                    \"claimID\": \"CLID1630486917871\",\n" +
                "\t\t\t\"primaryEmail\": \"jbutt@jktech.com\"\n" +
                "\t\t},\n" +
                "\t\t\"witnessDetails\": [\n" +
                "            {\n" +
                "\t\t\t\"witnessName\": \"Josephin\",\n" +
                "\t\t\t\"phone\": \"810-292-9388\",\n" +
                "\t\t\t\"email\": \"josephine_darakjy@jktech.com\",\n" +
                "\t\t\t\"address\": {\n" +
                "\t\t\t\t\"addressName\": \"1108  Warner Street\",\n" +
                "\t\t\t\t\"addressCity\": \"Miami\",\n" +
                "\t\t\t\t\"addressState\": 1,\n" +
                "\t\t\t\t\"addressCountry\": 1,\n" +
                "\t\t\t\t\"addressZip\": \"33139\"\n" +
                "\t\t\t}\n" +
                "\t\t}]\n" +
                "\t\t}\n" +
                "\t\t]\n" +
                "\t}\n" +
                "}";
        return json;
    }
    private String glUpdatePayload(){
        String json="{\n" +
                "    \"incidentDetails\": {\n" +
                "\t        \"incidentId\": \"I1630487658091\",\n" +
                "\n" +
                "            \"subClaimType\": 4,\n" +
                "\t\"claimsLists\":[\n" +
                "        {\n" +
                "        \"claimDetails\": {\n" +
                "\t\t \"claimID\": \"CLID1630487658237\",\n" +
                "                    \"incidentID\": \"I1630487658091\",\n" +
                "            \"claimType\": \"1\",\n" +
                "            \"claimStatus\": \"1\",\n" +
                "\t\t\t\"subClaimType\":4,\n" +
                "            \"claimSourceType\": \"1\"\n" +
                "        },\n" +
                "        \"documentDetails\": [{\n" +
                "\t\t \"documentID\": 158,\n" +
                "                        \"claimID\": \"CLID1630487658237\",\n" +
                "            \"documentTitle\": \"Claim Form\",\n" +
                "            \"documentURL\": \"https://picsum.photos/200/300\",\n" +
                "            \"documentType\": \"1\",\n" +
                "            \"documentFormat\": \"png\"\n" +
                "        }],\n" +
                "        \"glOccuranceDetails\": {\n" +
                "\t\t\"occuranceID\": 12,\n" +
                "                    \"claimID\": \"CLID1630487658237\",\n" +
                "            \"policeContacted\": \"No\",\n" +
                "            \"reportNumber\": \"\",\n" +
                "            \"occurrenceDescription\": \"\",        \n" +
                "            \"address\": {\n" +
                "                \"addressName\": \"1108  Warner Street\",\n" +
                "                \"addressCity\": \"Miami\",\n" +
                "                \"addressState\": 1,\n" +
                "                \"addressCountry\": 1,\n" +
                "                \"addressZip\": \"33139\"\n" +
                "            }\n" +
                "        },\n" +
                "        \"glLiabilityDetails\": {\n" +
                "\t\t\"liabilityID\": 11,\n" +
                "                    \"claimID\": \"CLID1630487658237\",\n" +
                "            \"premisesInsured\": \"Owner\",\n" +
                "            \"productInsured\": \"None\",\n" +
                "            \"typeOfPremises\": \"\"\n" +
                "        },\n" +
                "        \"glInjuredDetails\": {\n" +
                "\t\t \"injuredID\": 88,\n" +
                "                    \"claimID\": \"CLID1630487658237\",\n" +
                "            \"firstName\": \"John\",\n" +
                "            \"lastName\": \"Smith\",\n" +
                "            \"primaryPhone\": \"111-111-1111\",\n" +
                "            \"primaryPhoneType\":\"CELL\",\n" +
                "            \"primaryEmail\": \"john@xyz.com\",\n" +
                "            \"injuryCode\":18,\n" +
                "            \"injuryDescription\":\"\",\n" +
                "\t\t\t\"whereTaken\":\"\",\n" +
                "\t\t\t\"initialTreatment\":\"EMERGENCY CARE\",\n" +
                "            \"dob\": \"2016-08-29\",\n" +
                "            \"age\": 25,\n" +
                "            \"sex\": \"Male\",\n" +
                "            \"address\": {\n" +
                "                \"addressName\": \"1108  Warner Street\",\n" +
                "                \"addressCity\": \"Miami\",\n" +
                "                \"addressState\": 1,\n" +
                "                \"addressCountry\": 1,\n" +
                "                \"addressZip\": \"33139\"\n" +
                "            }\n" +
                "        },\n" +
                "        \"glClaimantDetails\": {\n" +
                "\t\t\"claimantID\": 117,\n" +
                "                    \"claimID\": \"CLID1630487658237\",\n" +
                "        \"primaryEmail\": \"macd@jktech.com\"\n" +
                "        },      \n" +
                "        \"witnessDetails\": [\n" +
                "            {\n" +
                "            \"witnessName\": \"Josephin\",\n" +
                "            \"phone\": \"810-292-9388\",\n" +
                "            \"email\": \"josephine_darakjy@jktech.com\",\n" +
                "            \"address\": {\n" +
                "                \"addressName\": \"1108  Warner Street\",\n" +
                "                \"addressCity\": \"Miami\",\n" +
                "                \"addressState\": 1,\n" +
                "                \"addressCountry\": 1,\n" +
                "                \"addressZip\": \"33139\"\n" +
                "            }\n" +
                "        }]\n" +
                "    },\n" +
                "\t\n" +
                "\t{\n" +
                "        \"claimDetails\": {\n" +
                "\t\t  \"claimID\": \"CLID1630487658375\",\n" +
                "                    \"incidentID\": \"I1630487658091\",\n" +
                "            \"claimType\": \"1\",\n" +
                "            \"claimStatus\": \"1\",\n" +
                "            \"claimSourceType\": \"1\",\n" +
                "\t\t\t\"subClaimType\":5\n" +
                "        },\n" +
                "        \"documentDetails\": [{\n" +
                "\t\t\"documentID\": 159,\n" +
                "                        \"claimID\": \"CLID1630487658375\",\n" +
                "            \"documentTitle\": \"Claim Form\",\n" +
                "            \"documentURL\": \"https://picsum.photos/200/300\",\n" +
                "            \"documentType\": \"1\",\n" +
                "            \"documentFormat\": \"png\"\n" +
                "        }],\n" +
                "        \"glOccuranceDetails\": {\n" +
                "\t\t\"occuranceID\": 13,\n" +
                "                    \"claimID\": \"CLID1630487658375\",\n" +
                "            \"policeContacted\": \"No\",\n" +
                "            \"reportNumber\": \"\",\n" +
                "            \"occurrenceDescription\": \"\",        \n" +
                "            \"address\": {\n" +
                "                \"addressName\": \"1108  Warner Street\",\n" +
                "                \"addressCity\": \"Miami\",\n" +
                "                \"addressState\": 1,\n" +
                "                \"addressCountry\": 1,\n" +
                "                \"addressZip\": \"33139\"\n" +
                "            }\n" +
                "        },\n" +
                "        \"glLiabilityDetails\": {\n" +
                "\t\t\"liabilityID\": 12,\n" +
                "                    \"claimID\": \"CLID1630487658375\",\n" +
                "            \"premisesInsured\": \"Owner\",\n" +
                "            \"productInsured\": \"None\",\n" +
                "            \"typeOfPremises\": \"\"\n" +
                "        },\n" +
                "        \"glClaimantDetails\": {\n" +
                "\t\t \"claimantID\": 118,\n" +
                "                    \"claimID\": \"CLID1630487658375\",\n" +
                "        \"primaryEmail\": \"macd@jktech.com\"\n" +
                "        },\n" +
                "        \"propertyDetails\": {\n" +
                "\t\t\"propertyID\": 53,\n" +
                "                    \"claimID\": \"CLID1630487658375\",\n" +
                "            \"firstName\": \"John\",\n" +
                "            \"lastName\": \"Smith\",\n" +
                "\t\t\t \"dob\": \"2016-08-29\",\n" +
                "            \"age\": 25,\n" +
                "            \"sex\": \"Male\",\n" +
                "            \"primaryPhone\": \"111-111-1111\",\n" +
                "            \"primaryPhoneType\":\"CELL\",\n" +
                "            \"primaryEmail\": \"jbutt@jktech.com\",\n" +
                "            \"describeProperty\": \"\",\n" +
                "            \"estimateAmount\": 100,\n" +
                "            \"wherePropertyCanBeSeen\": \"\",\n" +
                "            \"address\": {\n" +
                "                \"addressName\": \"1108  Warner Street\",\n" +
                "                \"addressCity\": \"Miami\",\n" +
                "                \"addressState\": 1,\n" +
                "                \"addressCountry\": 1,\n" +
                "                \"addressZip\": \"33139\"\n" +
                "            }\n" +
                "        },\n" +
                "        \"witnessDetails\": [\n" +
                "            {\n" +
                "            \"witnessName\": \"Josephin\",\n" +
                "            \"phone\": \"810-292-9388\",\n" +
                "            \"email\": \"josephine_darakjy@jktech.com\",\n" +
                "            \"address\": {\n" +
                "                \"addressName\": \"1108  Warner Street\",\n" +
                "                \"addressCity\": \"Miami\",\n" +
                "                \"addressState\": 1,\n" +
                "                \"addressCountry\": 1,\n" +
                "                \"addressZip\": \"33139\"\n" +
                "            }\n" +
                "        }]\n" +
                "    }\n" +
                "\t]\n" +
                "}\n" +
                "\t\n" +
                "}";
        return json;
    }


}