package com.jkt.fnolprocess.controller;

import com.jkt.fnolprocess.InsuranceClaimEngineApplicationTest;
import com.jkt.fnolprocess.entities.*;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
class LookupControllerTest extends InsuranceClaimEngineApplicationTest {

    @Autowired
    LookupController controller;

    @Test
    void getClaimType() {
        List<ClaimType> claimTypeList=controller.getClaimType();
        Assert.assertEquals("General Liability",claimTypeList.get(0).getClaimType());
    }

    @Test
    void getClaimStatus() {
        List<ClaimStatusDetails> claimStatusDetailsList=controller.getClaimStatus();
        Assert.assertEquals("Incident Reported",claimStatusDetailsList.get(0).getStatus());
    }

    @Test
    void getSourceType() {
        List<SourceType> sourceTypeList=controller.getSourceType();
        Assert.assertEquals("Email",sourceTypeList.get(1).getSourceType());
    }

    @Test
    void getDocumentType() {
        List<DocumentType> documentTypeList=controller.getDocumentType();
        Assert.assertEquals("Death certificate",documentTypeList.get(1).getDescription());
    }

    @Test
    void getMaritalStatus() {
        List<MaritalStatus> maritalStatusList=controller.getMaritalStatus();
        Assert.assertEquals("Married",maritalStatusList.get(1).getMaritalStatus());
    }

    @Test
    void getOccupations() {
        List<OccupationDetails> occupationDetailsList=controller.getOccupations();
        Assert.assertEquals("Actor",occupationDetailsList.get(1).getOccupationDescription());
    }

    @Test
    void getWagesFrequency() {
        List<WagesFrequencyDetails> wagesFrequencyDetailsList=controller.getWagesFrequency();
        Assert.assertEquals("Weekly",wagesFrequencyDetailsList.get(1).getWageDescription());
    }

    @Test
    void getInjuryCodes() {
        List<InjuryCodeDetails> injuryCodeDetailsList=controller.getInjuryCodes(1);
            Assert.assertEquals("11",injuryCodeDetailsList.get(1).getInjuryCode());
    }

    @Test
    void getAllInjuryCodes() {
        List<InjuryCodeDetails> injuryCodeDetailsList=controller.getAllInjuryCodes();
        Assert.assertEquals("11",injuryCodeDetailsList.get(1).getInjuryCode());
    }

    @Test
    void getBodyCodes() {
        List<BodyCodeDetails> bodyCodeDetailsList=controller.getBodyCodes(1);
        Assert.assertEquals("11",bodyCodeDetailsList.get(1).getBodyCode());
    }

    @Test
    void getAllBodyCodes() {
        List<BodyCodeDetails> bodyCodeDetailsList=controller.getAllBodyCodes();
        Assert.assertEquals("11",bodyCodeDetailsList.get(1).getBodyCode());
    }

    @Test
    void getNaicCodes() {
        List<NaicCodeDetails> naicCodeDetailsList=controller.getNaicCodes(1);
        Assert.assertEquals("111120",naicCodeDetailsList.get(1).getNaicCode());
    }

    @Test
    void getNcciCodes() {
        List<NcciCodeDetails> ncciCodeDetailsList=controller.getNcciCodes();
        Assert.assertEquals("6",ncciCodeDetailsList.get(1).getNcciCode());
    }

    @Test
    void getInjuryCauses() {
        List<InjuryCauseDetails> injuryCauseDetailsList=controller.getInjuryCauses();
        Assert.assertEquals("2",injuryCauseDetailsList.get(1).getInjuryCauseCode());
    }

    @Test
    void getIndustryCodes() {
        List<IndustryCodeDetails> industryCodeDetailsList=controller.getIndustryCodes();
        Assert.assertEquals("1120",industryCodeDetailsList.get(1).getIndustryCode());
    }

    @Test
    void getProductType() {
        List<ProductTypeDetails> productTypeDetailsList=controller.getProductType();
        Assert.assertEquals(true,productTypeDetailsList.size()>=0);
    }

    @Test
    void getPremisesType() {
        List<PremisesTypeDetails> premisesTypeDetailsList=controller.getPremisesType();
        Assert.assertEquals(true,premisesTypeDetailsList.size()>=0);
    }

    @Test
    void getCountries() {
        List<CountryDetails> countryDetailsList=controller.getCountries();
        Assert.assertEquals("US",countryDetailsList.get(0).getCountryName());
    }

    @Test
    void getStates() {
        List<StateDetails> stateDetailsList=controller.getStates(1);
        Assert.assertEquals("Alaska",stateDetailsList.get(1).getStateName());
    }

    @Test
    void getAllStates() {
        List<StateDetails> stateDetailsList=controller.getAllStates();
        Assert.assertEquals("Alaska",stateDetailsList.get(1).getStateName());
    }

    @Test
    void getSubClaimType() {
        List<SubClaimType> subClaimTypeList=controller.getSubClaimType();
        Assert.assertEquals("IO",subClaimTypeList.get(0).getSubClaimType());
    }
}