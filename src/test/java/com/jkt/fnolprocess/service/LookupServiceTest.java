package com.jkt.fnolprocess.service;

import com.jkt.fnolprocess.InsuranceClaimEngineApplicationTest;
import com.jkt.fnolprocess.entities.*;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
class LookupServiceTest extends InsuranceClaimEngineApplicationTest {

    @Autowired
    LookupService service;

    @Test
    void getClaimType() {
        try {
            List<ClaimType> claimTypeList = service.getClaimType();
            Assert.assertEquals(new ArrayList<>(),claimTypeList.get(claimTypeList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getClaimStatus() {
        try {
            List<ClaimStatusDetails> claimStatusDetailsList = service.getClaimStatus();
            Assert.assertEquals(new ArrayList<>(),claimStatusDetailsList.get(claimStatusDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getSourceType() {
        try {
            List<SourceType> sourceTypeList = service.getSourceType();
            Assert.assertEquals(new ArrayList<>(),sourceTypeList.get(sourceTypeList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getDocumentType() {
        try {
            List<DocumentType> documentTypeList = service.getDocumentType();
            Assert.assertEquals(new ArrayList<>(),documentTypeList.get(documentTypeList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getMaritalStatus() {
        try {
            List<MaritalStatus> maritalStatusList = service.getMaritalStatus();
            Assert.assertEquals(new ArrayList<>(),maritalStatusList.get(maritalStatusList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getOccupations() {
        try {
            List<OccupationDetails> occupationDetailsList = service.getOccupations();
            Assert.assertEquals(new ArrayList<>(),occupationDetailsList.get(occupationDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getWagesFrequency() {
        try {
            List<WagesFrequencyDetails> wagesFrequencyDetailsList = service.getWagesFrequency();
            Assert.assertEquals(new ArrayList<>(),wagesFrequencyDetailsList.get(wagesFrequencyDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getInjuryCodes() {
        try {
            List<InjuryCodeDetails> injuryCodeDetailsList = service.getInjuryCodes(1);
            Assert.assertEquals(new ArrayList<>(),injuryCodeDetailsList.get(injuryCodeDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getAllInjuryCodes() {
        try {
            List<InjuryCodeDetails> injuryCodeDetailsList = service.getAllInjuryCodes();
            Assert.assertEquals(new ArrayList<>(),injuryCodeDetailsList.get(injuryCodeDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getBodyCodes() {
        try {
            List<BodyCodeDetails> bodyCodeDetailsList = service.getBodyCodes(1);
            Assert.assertEquals(new ArrayList<>(),bodyCodeDetailsList.get(bodyCodeDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getAllBodyCodes() {
        try {
            List<BodyCodeDetails> bodyCodeDetailsList = service.getAllBodyCodes();
            Assert.assertEquals(new ArrayList<>(),bodyCodeDetailsList.get(bodyCodeDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getNaicCodes() {
        try {
            List<NaicCodeDetails> naicCodeDetailsList = service.getNaicCodes(1);
            Assert.assertEquals(new ArrayList<>(),naicCodeDetailsList.get(naicCodeDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getNcciCodes() {
        try {
            List<NcciCodeDetails> ncciCodeDetailsList = service.getNcciCodes();
            Assert.assertEquals(new ArrayList<>(),ncciCodeDetailsList.get(ncciCodeDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getInjuryCauses() {
        try {
            List<InjuryCauseDetails> injuryCauseDetailsList = service.getInjuryCauses();
            Assert.assertEquals(new ArrayList<>(),injuryCauseDetailsList.get(injuryCauseDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getIndustryCodes() {
        try {
            List<IndustryCodeDetails> industryCodeDetailsList = service.getIndustryCodes();
            Assert.assertEquals(new ArrayList<>(),industryCodeDetailsList.get(industryCodeDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getProductType() {
        try {
            List<ProductTypeDetails> productTypeDetailsList = service.getProductType();
            Assert.assertEquals(new ArrayList<>(),productTypeDetailsList.get(productTypeDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getPremisesType() {
        try {
            List<PremisesTypeDetails> premisesTypeDetailsList = service.getPremisesType();
            Assert.assertEquals(new ArrayList<>(),premisesTypeDetailsList.get(premisesTypeDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getCountries() {
        try {
            List<CountryDetails> countryDetailsList = service.getCountries();
            Assert.assertEquals(new ArrayList<>(),countryDetailsList.get(countryDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getStates() {
        try {
            List<StateDetails> stateDetailsList = service.getStates(1);
            Assert.assertEquals(new ArrayList<>(),stateDetailsList.get(stateDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getAllStates() {
        try {
            List<StateDetails> stateDetailsList = service.getAllStates();
            Assert.assertEquals(new ArrayList<>(),stateDetailsList.get(stateDetailsList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    void getSubClaimType() {
        try {
            List<SubClaimType> subClaimTypeList = service.getSubClaimType();
            Assert.assertEquals(new ArrayList<>(),subClaimTypeList.get(subClaimTypeList.size()));
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }
}