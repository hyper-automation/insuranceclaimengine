package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;

@Table(name = "GL_injuredDetails")
@Entity
@Getter
@Setter
@Data
public class GLInjuredDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer injuredID;
    @Column
    private String claimID;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    private Date dob;
    @Column
    private Long age;
    @Column
    private String sex;
    @Column
    private Integer injuryCode;
    @Column
    private String injuryDescription;
    @Column
    private  String primaryPhone;
    @Column
    private String primaryPhoneType;
    @Column
    private String secondaryPhone;
    @Column
    private String secondaryPhoneType;
    @Column
    private String primaryEmail;
    @Column
    private String whereTaken;
    @Column
    private String initialTreatment;
    @Column
    private String secondaryEmail;
    @OneToOne(cascade = CascadeType.ALL , fetch = FetchType.EAGER , optional = false)
    @JoinColumn(name = "address")
    private AddressDetails address;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private LocalDateTime modifiedOn;
}
