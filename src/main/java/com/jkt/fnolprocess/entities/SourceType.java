package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name="sourceType")
@Getter
@Setter
@Data
public class SourceType implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer sourceTypeID;
    @Column
    private String sourceType;
    @Column
    private String sourceDescription;

}
