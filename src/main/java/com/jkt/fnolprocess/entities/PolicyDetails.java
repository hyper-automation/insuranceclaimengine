package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "policyDetails")
@Entity
@Data
@Getter
@Setter
public class PolicyDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer policyID;
    @Column
    private String policyNumber;
    @Column
    private String policyDescription;
    @Column
    private String policyCategory;
    @Column
    private Integer employer;
}
