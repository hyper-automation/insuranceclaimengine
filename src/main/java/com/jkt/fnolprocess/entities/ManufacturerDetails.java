package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Table(name = "manufacturerDetails")
@Entity
@Getter
@Setter
@Data
public class ManufacturerDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer manufacturerID;
    @Column
    private String manufacturerName;
    @OneToOne(cascade = CascadeType.ALL , fetch = FetchType.EAGER , optional = false)
    @JoinColumn(name = "address")
    private AddressDetails address;
    @Column
    private Integer productType;
    @Column
    private Integer premisesType;
    @Column
    private String phone;
    @Column
    private String email;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private LocalDateTime modifiedOn;
}
