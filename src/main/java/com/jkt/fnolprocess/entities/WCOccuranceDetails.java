package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Table(name = "WC_occuranceDetails")
@Entity
@Getter
@Setter
@Data
public class WCOccuranceDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer occuranceID;

    @Column
    private String claimID;

@OneToOne(cascade = CascadeType.ALL , fetch = FetchType.EAGER , optional = false)
 @JoinColumn(name = "address")
    private AddressDetails address;
    @Column
    private String otherAdress;
    @Column
    private Date lastWorkDate;
    @Column
    private Date employerNotifiedDate;
    @Column
    private String didOccuredOnEmpPremises;
    @Column
    private String occurredDepartment;
    @Column
    private String equipmentList;
    @Column
    private String specificActivity;
    @Column
    private String safetyEqupProvided;
    @Column
    private String safetyEqupUsed;
    @Column
    private String occurrenceDetails;
    @Column
    private String workProcess;
    @Column
    private String eventSequence;
    @Column
    private  Date  ocurrenceTime;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private LocalDateTime modifiedOn;

}
