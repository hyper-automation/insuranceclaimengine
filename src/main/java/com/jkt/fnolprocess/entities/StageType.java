package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "stageType")
@Getter
@Setter
@Data
public class StageType implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer stageTypeID;
    @Column
    private String description;

}
