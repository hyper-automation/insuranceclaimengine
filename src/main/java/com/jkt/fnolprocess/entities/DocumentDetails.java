package com.jkt.fnolprocess.entities;

import com.jkt.fnolprocess.domains.DocumentDetailsPojo;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Table(name = "documentDetails")
@Data
public class DocumentDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer documentID;
    @Column
    private String claimID;
    @Column
    private String documentTitle;
    @Column
    private String documentURL;
    @Column
    private Integer documentType;
    @Column
    private String documentFormat;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private LocalDateTime modifiedOn;

    public  static  DocumentDetails getEntity(DocumentDetailsPojo pojo){
        DocumentDetails documentDetails=new DocumentDetails();
        documentDetails.setDocumentTitle(pojo.getDocumentTitle());
        documentDetails.setDocumentURL(pojo.getDocumentURL());
        documentDetails.setDocumentType(pojo.getDocumentType());
        documentDetails.setDocumentFormat(pojo.getDocumentFormat());
        return documentDetails;
    }
}
