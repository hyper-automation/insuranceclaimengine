package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;

@Table(name = "vendorDetails")
@Entity
@Getter
@Setter
@Data
public class VendorDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer vendorID;
    @Column
    private String vendorName;
    @OneToOne(cascade = CascadeType.ALL , fetch = FetchType.EAGER , optional = false)
    @JoinColumn(name = "address")
    private AddressDetails address;
    @Column
    private Long productType;
    @Column
    private Long premisesType;
    @Column
    private String phone;
    @Column
    private String email;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private LocalDateTime modifiedOn;
}
