package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.time.LocalDateTime;
import java.util.Date;

@Table(name = "WC_claimantDetails")
@Entity
@Getter
@Setter
@Data
public class WCClaimantDetails {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer claimantID;
    @Column
    private String claimID;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    private Date dob;
    @Column
    private Integer age;
    @Column
    private String sex;
    @Column
    private Integer maritalstatus;
    @Column
    private Integer occupation;
    @Column
    private String employementStatus;
    @Column
    private String feinNumber;
    @Column
    private String primaryPhone;
    @Column
    private String secondaryPhone;
    @Column
    private String primaryEmail;
    @Column
    private String secondaryEmail;
    @Column
    private Integer numOfDependents;
    @Column
    private  String employeeCode;
    @Column
    private String policyCategory;
    @Column
    private String SSN_Number;
    @Column
    private Date dateHired;
    @Column
    private String stateHired;
    @Column
    private Long ncciCodeID;
    @Column
    private Long wageRate;
    @Column
    private Long wageFrequencyID;
    @Column
    private Integer avgWeeklyWages;
    @Column
    private Integer noOfDaysPerWeek;
    @Column
    private String fullPayDayOfInjury;
    @Column
    private String salaryStatus;
    @Column
    private String whereTaken;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private LocalDateTime modifiedOn;
}
