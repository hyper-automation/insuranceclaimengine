package com.jkt.fnolprocess.entities;

import com.jkt.fnolprocess.domains.ClaimDetailsPojo;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
@Entity
@Table(name="claimDetails")
@Getter
@Setter
@Data
public class ClaimDetails implements Serializable {
    @Id
    @Column
    private String claimID;
    @Column
    private String incidentID;
    @Column
    private String assignedTo;
    @Column
    private Integer stageTypeID;
    @Column
    private Integer claimCategoryId;
    @Column
    private Integer subClaimType;
    @Column
    private Integer claimType;
    @Column
    private String policyNumber;
    @Column
    private Integer claimStatus;
    @Column
    private String ISOID;
    @Column
    private Integer claimSourceType;
    @Column
    private Integer emailClaimId;
    @Column
    private String claimCreatedBy;
    @Column
    private LocalDateTime claimCreatedOn;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private LocalDateTime modifiedOn;

    public static ClaimDetails getEntity(ClaimDetailsPojo pojo){
    ClaimDetails claimDetails=new ClaimDetails();
    claimDetails.setClaimType(pojo.getClaimType());
    claimDetails.setClaimSourceType(pojo.getClaimSourceType());
    claimDetails.setClaimStatus(pojo.getClaimStatus());
    return claimDetails;
    }
}
