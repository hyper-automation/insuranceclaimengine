package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import org.springframework.context.annotation.Scope;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;

@Getter
@Scope
@Table(name = "GL_occuranceDetails")
@Entity
@Data
public class GLOccuranceDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer occuranceID;
    @Column
    private String claimID;
    @OneToOne(cascade = CascadeType.ALL , fetch = FetchType.EAGER , optional = false)
    @JoinColumn(name = "address")
    private AddressDetails address;
    @Column
    private String otherAdress;
    @Column
    private String policeContacted;
    @Column
    private String reportNumber;
    @Column
    private String occurrenceDescription;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private LocalDateTime modifiedOn;
}
