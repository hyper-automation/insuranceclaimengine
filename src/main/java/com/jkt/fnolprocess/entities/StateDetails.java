package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "stateDetails")
@Entity
@Getter
@Setter
@Data
public class StateDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long stateID;
    @Column
    private String stateName;
    @Column
    private Integer country;
}
