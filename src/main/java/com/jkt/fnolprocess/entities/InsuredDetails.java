package com.jkt.fnolprocess.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "insuredDetails")
@Entity
@Getter
@Setter
public class InsuredDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer insuredID;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    private Date dob;
    @Column
    private Integer age;
    @Column
    private String sex;
    @Column
    private Integer maritalstatus;
    @Column
    private Integer occupation;
    @Column
    private String policyCategory;
    @Column
    private String feinNumber;
    @Column
    private String primaryPhone;
    @Column
    private String secondaryPhone;
    @Column
    private String primaryEmail;
    @Column
    private String secondaryEmail;
    @Column
    private Integer numOfDependents;
    @Column
    private String whereTaken;
    @Column
    private Integer employerId;
}
