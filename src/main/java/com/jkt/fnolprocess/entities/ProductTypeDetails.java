package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "productTypeDetails")
@Entity
@Getter
@Setter
@Data
public class ProductTypeDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer productTypeID;
    @Column
    private String productType;
}
