package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Table(name = "GL_DamagedPropertyDetails")
@Entity
@Getter
@Setter
@Data
public class PropertyDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer propertyID;
    @Column
    private String claimID;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    private Date dob;
    @Column
    private Integer age;
    @Column
    private String sex;
    @OneToOne(cascade = CascadeType.ALL , fetch = FetchType.EAGER , optional = false)
    @JoinColumn(name = "address")
    private AddressDetails address;
    @Column
    private  String primaryPhone;
    @Column
    private String primaryPhoneType;
    @Column
    private String secondaryPhone;
    @Column
    private String secondaryPhoneType;
    @Column
    private String primaryEmail;
    @Column
    private String secondaryEmail;
    @Column
    private  String describeProperty;
    @Column
    private Integer estimateAmount;
    @Column
    private String wherePropertyCanBeSeen;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private LocalDateTime modifiedOn;
}
