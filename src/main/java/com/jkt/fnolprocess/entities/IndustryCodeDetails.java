package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "industryCodeDetails")
@Getter
@Setter
@Data
public class IndustryCodeDetails implements Serializable {
    @Id
    @Column
    private Integer industryCodeID;
    @Column
    private String industryCode;
    @Column
    private String industryCodeDesc;
}
