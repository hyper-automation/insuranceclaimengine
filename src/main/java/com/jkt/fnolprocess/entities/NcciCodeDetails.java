package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
@Table(name = "ncciCodeDetails")
@Entity
@Getter
@Setter
@Data
public class NcciCodeDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ncciCodeID;
    @Column
    private String ncciCode;
}
