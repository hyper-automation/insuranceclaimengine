package com.jkt.fnolprocess.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;

@Entity
@Table(name = "claimType")
@Getter
@Setter
@Data
public class ClaimType implements Serializable {
    @Id
    @Column(name = "claimTypeId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer claimTypeId;

    @Column(name = "claimType")
    private String claimType;

    @Column(name = "claimDescription")
    private String claimDescription;
}
