package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "employeeDetails")
@Getter
@Setter
@Data
public class EmployeeDetails {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer employeeID;
    @Column
    private Integer insuredID;
    @Column
    private String employementStatus;
    @Column
    private String employeeCode;
    @Column
    private String SSN_Number;
    @Column
    private Date dateHired;
    @Column
    private String stateHired;
    @Column
    private Long ncciCodeID;
    @Column
    private Integer employerID;
    @Column
    private Long wageRate;
    @Column
    private  Long wageFrequencyID;
    @Column
    private Integer avgWeeklyWages;
    @Column
    private Integer noOfDaysPerWeek;
    @Column
    private String fullPayDayOfInjury;
    @Column
    private String salaryStatus;
}
