package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "injuryCodeDetails")
@Entity
@Getter
@Setter
@Data
public class InjuryCodeDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer injuryCodeID;
    @Column
    private String injuryCode;
    @Column
    private String injuryCodeDesc;
    @Column
    private Integer injuryCodeCategoryID;
}
