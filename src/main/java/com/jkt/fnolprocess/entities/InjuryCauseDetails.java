package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.naming.directory.SearchResult;
import javax.persistence.*;
import java.io.Serializable;

@Table(name = "injuryCauseDetails")
@Entity
@Getter
@Setter
@Data
public class InjuryCauseDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer injuryCauseID;
    @Column
    private  String injuryCauseCode;
    @Column
    private  String injuryCauseDesc;
}
