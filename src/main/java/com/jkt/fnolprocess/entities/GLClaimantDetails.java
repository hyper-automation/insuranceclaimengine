package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Table(name = "GL_claimantDetails")
@Getter
@Setter
@Entity
@Data
public class GLClaimantDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer claimantID;
    @Column
    private String claimID;
    @Column
    private String insuredName;
    @Column
    private Date dateOfIncorporation;
    @Column
    private String employementStatus;
    @Column
    private String feinNumber;
    @Column
    private String primaryPhone;
    @Column
    private String secondaryPhone;
    @Column
    private String primaryEmail;
    @Column
    private String secondaryEmail;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private LocalDateTime modifiedOn;
}
