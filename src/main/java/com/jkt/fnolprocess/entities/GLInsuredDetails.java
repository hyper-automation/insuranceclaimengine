package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "GL_insuredDetails")
@Entity
@Getter
@Setter
@Data
public class GLInsuredDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer insuredID;
    @Column
    private String insuredName;
    @Column
    private Date dateOfIncorporation;
    @Column
    private Integer employerID;
    @Column
    private String policyCategory;
    @Column
    private String feinNumber;
    @Column
    private  String primaryPhone;
    @Column
    private String primaryPhoneType;
    @Column
    private String secondaryPhone;
    @Column
    private String secondaryPhoneType;
    @Column
    private String primaryEmail;
    @Column
    private String secondaryEmail;
    @Column
    private String addressID;

}
