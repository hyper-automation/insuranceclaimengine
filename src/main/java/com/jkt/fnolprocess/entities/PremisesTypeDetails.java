package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "premisesTypeDetails")
@Getter
@Setter
@Data
public class PremisesTypeDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer premisesTypeID;
    @Column
    private  String premisesType;
}
