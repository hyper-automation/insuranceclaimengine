package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Table(name = "GL_liabilityDetails")
@Entity
@Getter
@Setter
@Data
public class GLLiabilityDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer liabilityID;
    @Column
    private String claimID;
    @Column
    private String premisesInsured;
    @Column
    private String productInsured;
    @Column
    private String typeOfPremises;
    @OneToOne(cascade = CascadeType.ALL , fetch = FetchType.EAGER , optional = true)
    @JoinColumn(name = "vendor")
    private VendorDetails vendor;
    @OneToOne(cascade = CascadeType.ALL , fetch = FetchType.EAGER , optional = true)
    @JoinColumn(name = "manufactuter")
    private ManufacturerDetails manufactuter;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private LocalDateTime modifiedOn;
}
