package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "bodyCodeDetails")
@Entity
@Getter
@Setter
@Data
public class BodyCodeDetails implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer bodyCodeID;
    @Column
    private String bodyCode;
    @Column
    private String bodyCodeDesc;
    @Column
    private Integer bodyCodeCategoryID;
}
