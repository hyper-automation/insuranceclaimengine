package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "subClaimType")
@Getter
@Setter
@Data
public class SubClaimType implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer subClaimTypeID;
    @Column
    private String subClaimType;
    @Column
    private String subClaimDescription;
}
