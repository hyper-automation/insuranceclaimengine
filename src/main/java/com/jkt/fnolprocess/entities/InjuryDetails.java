package com.jkt.fnolprocess.entities;

import com.jkt.fnolprocess.domains.InjuryDetailsPojo;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@Table(name = "injuryDetails")
@Entity
@Data
public class InjuryDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer injuryID;
    @Column
    private String claimID;
    @Column
    private Date injuryDate;
    @Column
    private Integer injuryCause;
    @Column
    private Integer injuryType;
    @Column
    private Integer bodyCode;
    @Column
    private String fatalStatus;
    @Column
    private Date deathDate;
    @Column
    private String beginWork;
    @Column
    private Date returnDate;
    @Column
    private Date disabilityBeganDate;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private LocalDateTime modifiedOn;

    public static InjuryDetails getEntity(InjuryDetailsPojo pojo){
        InjuryDetails injuryDetails = new InjuryDetails();
        injuryDetails.setInjuryDate(pojo.getInjuryDate());
        injuryDetails.setInjuryType(pojo.getInjuryType());
        injuryDetails.setInjuryCause(pojo.getInjuryCause());
        injuryDetails.setBodyCode(pojo.getBodyCode());
        injuryDetails.setFatalStatus(StringUtils.isBlank(pojo.getFatalStatus())?"No": pojo.getFatalStatus());
        injuryDetails.setDeathDate(pojo.getDeathDate());
        injuryDetails.setBeginWork(pojo.getBeginWork());
        injuryDetails.setReturnDate(pojo.getReturnDate());
        injuryDetails.setDisabilityBeganDate(pojo.getDisabilityBeganDate());
        injuryDetails.setStatus("Active");
        injuryDetails.setModifiedOn(LocalDateTime.now());

        return injuryDetails;
    }
    public static InjuryDetailsPojo toPojo(InjuryDetails injuryDetails){
        InjuryDetailsPojo injuryDetailsPojo = new InjuryDetailsPojo();
        injuryDetailsPojo.setInjuryID(injuryDetails.getInjuryID());
        injuryDetailsPojo.setClaimID(injuryDetails.getClaimID());
        injuryDetailsPojo.setInjuryDate(injuryDetails.getInjuryDate());
        injuryDetailsPojo.setInjuryType(injuryDetails.getInjuryType());
        injuryDetailsPojo.setInjuryCause(injuryDetails.getInjuryCause());
        injuryDetailsPojo.setBodyCode(injuryDetails.getBodyCode());
        injuryDetailsPojo.setFatalStatus(injuryDetails.getFatalStatus());
        injuryDetailsPojo.setDeathDate(injuryDetails.getDeathDate());
        injuryDetailsPojo.setBeginWork(injuryDetails.getBeginWork());
        injuryDetailsPojo.setReturnDate(injuryDetails.getReturnDate());
        injuryDetailsPojo.setDisabilityBeganDate(injuryDetails.getDisabilityBeganDate());
        injuryDetailsPojo.setStatus("Active");
        injuryDetailsPojo.setModifiedBy(injuryDetails.getModifiedBy());
        injuryDetailsPojo.setModifiedOn(injuryDetails.getModifiedOn());
        return injuryDetailsPojo;
    }
}
