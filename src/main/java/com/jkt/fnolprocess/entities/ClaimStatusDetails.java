package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;

@Table(name = "claimStatusDetails")
@Entity
@Getter
@Setter
@Data
public class ClaimStatusDetails implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer statusID;
    @Column
    private String status;
    @Column
    private String statusDescription;

}
