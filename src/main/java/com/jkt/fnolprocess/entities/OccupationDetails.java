package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name = "occupationDetails")
@Getter
@Setter
@Data
public class OccupationDetails implements Serializable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer occupationID;
    @Column
    private String occupationDescription;
}
