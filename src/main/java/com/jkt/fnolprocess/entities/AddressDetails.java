package com.jkt.fnolprocess.entities;

import com.jkt.fnolprocess.domains.Address;


import javax.persistence.*;
import java.io.Serializable;

@Table(name = "addressDetails")
@Entity
public class AddressDetails implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer addressID;
    @Column
    private String addressName;
    @Column
    private String addressCity;
    @Column
    private Integer addressState;
    @Column
    private Integer addressCountry;
    @Column
    private String addressZip;

    public Integer getAddressID() {
        return addressID;
    }

    public void setAddressID(Integer addressID) {
        this.addressID = addressID;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public Integer getAddressState() {
        return addressState;
    }

    public void setAddressState(Integer addressState) {
        this.addressState = addressState;
    }

    public Integer getAddressCountry() {
        return addressCountry;
    }

    public void setAddressCountry(Integer addressCountry) {
        this.addressCountry = addressCountry;
    }

    public String getAddressZip() {
        return addressZip;
    }

    public void setAddressZip(String addressZip) {
        this.addressZip = addressZip;
    }

    public static AddressDetails getEntity(Address pojo) {
        AddressDetails addressDetails=new AddressDetails();
        addressDetails.setAddressName(pojo.getAddressName());
        addressDetails.setAddressCountry(pojo.getAddressCountry());
        addressDetails.setAddressCity(pojo.getAddressCity());
        addressDetails.setAddressState(pojo.getAddressState());
        addressDetails.setAddressZip(pojo.getAddressZip());
    return addressDetails;
    }
}