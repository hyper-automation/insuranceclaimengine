package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "wagesFrequencyDetails")
@Getter
@Setter
@Data
public class WagesFrequencyDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer wageFrequencyID;
    @Column
    private String wageDescription;
}
