package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "incidentDetails")
@Getter
@Setter
@Data
public class IncidentDetails implements Serializable {
    @Id
    @Column
    private String incidentID;
    @Column
    private Integer subClaimType;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private LocalDateTime modifiedOn;

}
