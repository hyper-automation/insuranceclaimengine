package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "maritalStatus")
@Getter
@Setter
@Data
public class MaritalStatus implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer maritalStatusID;
    @Column
    private String maritalStatus;

}
