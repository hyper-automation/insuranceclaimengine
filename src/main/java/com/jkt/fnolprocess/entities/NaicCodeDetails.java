package com.jkt.fnolprocess.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "naicCodeDetails")
@Entity
@Getter
@Setter
@Data
public class NaicCodeDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer naicCodeID;
    @Column
    private String naicCode;
    @Column
    private  String naicCodeDesc;
    @Column
    private Integer naicCodeCategoryID;
}
