package com.jkt.fnolprocess.service;

import com.jkt.fnolprocess.entities.*;
import com.jkt.fnolprocess.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LookupService {
    @Autowired
    ClaimTypeRepository claimTypeRepository;
    @Autowired
    ClaimStatusRepository claimStatusRepository;
    @Autowired
    SourceTypeRepository sourceTypeRepository;
    @Autowired
    DocumentTypeRepository documentTypeRepository;
    @Autowired
    MaritalStatusRepository maritalStatusRepository;
    @Autowired
    OccupationRepository occupationRepository;
    @Autowired
    WagesFrequencyRepository wagesFrequencyRepository;
    @Autowired
    InjuryCauseRepository injuryCauseRepository;

    @Autowired
    IndustryCodeDetailsRepository industryCodeDetailsRepository;

    @Autowired
    InjuryCodeRepository injuryCodeRepository;
    @Autowired
    NcciCodeRepository ncciCodeRepository;
    @Autowired
    NaicCodeRepository naicCodeRepository;
    @Autowired
    ProductTypeRepository productTypeRepository;
    @Autowired
    PremisesTypeRepository premisesTypeRepository;
    @Autowired
    StateRepository stateRepository;
    @Autowired
    CountryRepository countryRepository;
    @Autowired
    BodyCodeRepository bodyCodeRepository;
    @Autowired
    SubClaimTypeRepository subClaimTypeRepository;


    public List<ClaimType> getClaimType() {
        return claimTypeRepository.findAll();
    }

    public List<ClaimStatusDetails> getClaimStatus() {
        return claimStatusRepository.findAll();
    }

    public List<SourceType> getSourceType() {
        return sourceTypeRepository.findAll();
    }

    public List<DocumentType> getDocumentType() {
        return documentTypeRepository.findAll();
    }

    public List<MaritalStatus> getMaritalStatus() {
        return maritalStatusRepository.findAll();

    }

    public List<OccupationDetails> getOccupations() {
        return occupationRepository.findAll();

    }

    public List<WagesFrequencyDetails> getWagesFrequency() {
        return wagesFrequencyRepository.findAll();

    }

    public List<InjuryCodeDetails> getInjuryCodes(Integer injuryCodeCategoryID) {
        return injuryCodeRepository.findByInjuryCodeCategoryID(injuryCodeCategoryID);
    }

    public List<InjuryCodeDetails> getAllInjuryCodes() {
        return injuryCodeRepository.findAll();
    }

    public List<BodyCodeDetails> getBodyCodes(Integer bodyCodeCategoryID) {
        return bodyCodeRepository.findByBodyCodeCategoryID(bodyCodeCategoryID);
    }

    public List<BodyCodeDetails> getAllBodyCodes() {
        return bodyCodeRepository.findAll();
    }

    public List<NaicCodeDetails> getNaicCodes(Integer naicCodeCategoryID) {
        return naicCodeRepository.findByNaicCodeCategoryID(naicCodeCategoryID);
    }

    public List<NcciCodeDetails> getNcciCodes() {
        return ncciCodeRepository.findAll();

    }

    public List<InjuryCauseDetails> getInjuryCauses() {
        return injuryCauseRepository.findAll();

    }

    public List<IndustryCodeDetails> getIndustryCodes() {
        return industryCodeDetailsRepository.findAll();

    }

    public List<ProductTypeDetails> getProductType() {
        return productTypeRepository.findAll();
    }

    public List<PremisesTypeDetails> getPremisesType() {
        return premisesTypeRepository.findAll();
    }

    public List<CountryDetails> getCountries() {
        return countryRepository.findAll();
    }

    public List<StateDetails> getStates(Integer country) {
        return stateRepository.findBycountry(country);

    }

    public List<StateDetails> getAllStates() {
        return stateRepository.findAll();


    }

    public List<SubClaimType> getSubClaimType() {
        return subClaimTypeRepository.findAll();

    }
}
