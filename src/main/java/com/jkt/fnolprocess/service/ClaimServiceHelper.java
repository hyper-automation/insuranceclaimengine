package com.jkt.fnolprocess.service;

import com.jkt.fnolprocess.domains.Claims;
import com.jkt.fnolprocess.domains.IncidentDetailsPojo;
import com.jkt.fnolprocess.entities.*;
import com.jkt.fnolprocess.repositories.FnolProcessRepository;
import com.jkt.fnolprocess.service.module.ClaimDetailService;
import com.jkt.fnolprocess.util.UtilityDTO;
import com.jkt.fnolprocess.service.module.IncidentDetailsService;
import com.jkt.fnolprocess.util.ClaimSourceTypeEnum;
import com.jkt.fnolprocess.util.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ClaimServiceHelper {

    @Autowired
    IncidentDetailsService incidentDetailsService;

    @Autowired
    FnolProcessRepository fnolProcessRepository;

    @Autowired
    ClaimDetailService claimDetailService;


    public ResponseDTO saveClaim(IncidentDetailsPojo incidentDetailsPojo, ClaimSourceTypeEnum claimSourceTypeEnum){
        ResponseDTO responseDTO=new ResponseDTO();
        UtilityDTO utilityDTO = new UtilityDTO();
        if (incidentDetailsPojo.getClaimsLists().get(0).getClaimDetails().getClaimType() == 2) {
            utilityDTO.createWCDefault(claimSourceTypeEnum);
            utilityDTO.setEmail(incidentDetailsPojo.getClaimsLists().get(0).getWcClaimantDetails().getPrimaryEmail());
            responseDTO.setClaimType("WC");
        }
        else if (incidentDetailsPojo.getClaimsLists().get(0).getClaimDetails().getClaimType() == 1) {
            utilityDTO.createGLDefault(claimSourceTypeEnum);
            utilityDTO.setEmail(incidentDetailsPojo.getClaimsLists().get(0).getGlClaimantDetails().getPrimaryEmail());
            responseDTO.setClaimType("GL");
        }
        incidentDetailsService.save(incidentDetailsPojo, utilityDTO);
        if(!utilityDTO.isActionSuccess()){
            responseDTO.setMessages(utilityDTO.getAllMessages());

        }else{
            responseDTO.add("claim created");
            responseDTO.getMessages().addAll(utilityDTO.getAllMessages());
            responseDTO.setIncidentId(utilityDTO.getIncidentId());

        }
        return responseDTO;
    }
    public Claims getClaim(String incidentId){
        IncidentDetailsPojo incidentDetailsPojo = new IncidentDetailsPojo();
        UtilityDTO utilityDTO = new UtilityDTO();
        utilityDTO.setIncidentId(incidentId);
        incidentDetailsService.map(incidentDetailsPojo, utilityDTO);
       return new Claims().setIncidentDetails(incidentDetailsPojo);
    }
    public ResponseDTO updateClaim(IncidentDetailsPojo incidentDetailsPojo) {
        ResponseDTO responseDTO=new ResponseDTO();
        UtilityDTO utilityDTO = new UtilityDTO();
        utilityDTO.setIncidentId(incidentDetailsPojo.getIncidentId());
        if (incidentDetailsPojo.getClaimsLists().get(0).getClaimDetails().getClaimType() == 2) {
            utilityDTO.createWCDefault(ClaimSourceTypeEnum.DEFAULT);
            utilityDTO.setEmail(incidentDetailsPojo.getClaimsLists().get(0).getWcClaimantDetails().getPrimaryEmail());
        }
        else if (incidentDetailsPojo.getClaimsLists().get(0).getClaimDetails().getClaimType() == 1) {
            utilityDTO.createGLDefault(ClaimSourceTypeEnum.DEFAULT);
            utilityDTO.setEmail(incidentDetailsPojo.getClaimsLists().get(0).getGlClaimantDetails().getPrimaryEmail());
        }
        incidentDetailsService.update(incidentDetailsPojo, utilityDTO);
        responseDTO.add("claim updated");
        responseDTO.getMessages().addAll(utilityDTO.getAllMessages());
        responseDTO.setIncidentId(utilityDTO.getIncidentId());
        return responseDTO;
    }

    @Transactional
    public List<Claims> getByEmployee(Integer employeeId){
        try {
            List<String> incidentIDList = fnolProcessRepository.getIncidentIDListByEmployee(employeeId);
            return incidentIDList.stream()
                    .map(this::getClaim)
                    .collect(Collectors.toList());
        }
        catch (Exception e){
            return new ArrayList<>();
        }
    }
    @Transactional
    public List<Claims> getByEmployer(Integer employerId){
        try {
            List<String> incidentIDList = fnolProcessRepository.getIncidentIDListByEmployer(employerId);
            return incidentIDList.stream()
                    .map(this::getClaim)
                    .collect(Collectors.toList());
        } catch (Exception e){
            return new ArrayList<>();
        }
    }
    @Transactional
    public List<Claims> getByInsured(Integer insuredID){
        try {
            List<String> incidentIDList = fnolProcessRepository.getIncidentIDListByInsuredID(insuredID);
            return incidentIDList.stream()
                    .map(this::getClaim)
                    .collect(Collectors.toList());
        } catch (Exception e){
            return new ArrayList<>();
        }
    }

    public List<Claims> getAllClaims(){
        try {
            List<IncidentDetails> incidentDetailsList = incidentDetailsService.getAllIncidentDetails();
            return incidentDetailsList.stream()
                    .map(incidentDetails -> getClaim(incidentDetails.getIncidentID()))
                    .collect(Collectors.toList());
        } catch (Exception e){
            return new ArrayList<>();
        }
    }
}

