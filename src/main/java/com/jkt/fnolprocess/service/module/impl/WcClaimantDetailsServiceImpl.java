package com.jkt.fnolprocess.service.module.impl;

import com.jkt.fnolprocess.domains.ClaimsList;
import com.jkt.fnolprocess.domains.IncidentDetailsPojo;
import com.jkt.fnolprocess.entities.EmployeeDetails;
import com.jkt.fnolprocess.entities.InsuredDetails;
import com.jkt.fnolprocess.entities.WCClaimantDetails;
import com.jkt.fnolprocess.repositories.EmployeeRepository;
import com.jkt.fnolprocess.repositories.InsuredDetailsRepository;
import com.jkt.fnolprocess.repositories.WcClaimantDetailsRepository;
import com.jkt.fnolprocess.service.module.WcClaimantDetailsService;
import com.jkt.fnolprocess.service.module.WitnessDetailsService;
import com.jkt.fnolprocess.util.ModuleStatus;
import com.jkt.fnolprocess.util.UtilityDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Slf4j
public class WcClaimantDetailsServiceImpl implements WcClaimantDetailsService {

    @Autowired
    WcClaimantDetailsRepository repository;

    @Autowired
    InsuredDetailsRepository insuredDetailsRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    WitnessDetailsService witnessDetailsService;

    @Override
    public void save(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasWcClaimantDetails() && claimsList.getWcClaimantDetails() != null) {
                WCClaimantDetails wcClaimantDetails = claimsList.getWcClaimantDetails();
                saveToDB(utilityDTO, wcClaimantDetails);
                utilityDTO.add(ModuleStatus.success("WC Claimant details saved"));
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("WC Claimant details save failed :: " + e.getMessage()));
        }
        witnessDetailsService.save(claimsList, utilityDTO);
    }

    private void saveToDB(UtilityDTO utilityDTO, WCClaimantDetails wcClaimantDetails) {
        InsuredDetails insuredDetails = insuredDetailsRepository.findByPrimaryEmail(wcClaimantDetails.getPrimaryEmail()).orElse(null);
        EmployeeDetails employeeDetails = employeeRepository.findByInsuredID(insuredDetails.getInsuredID()).orElse(null);
        wcClaimantDetails.setClaimID(utilityDTO.getClaimId());
        wcClaimantDetails.setFirstName(insuredDetails.getFirstName());
        wcClaimantDetails.setLastName(insuredDetails.getLastName());
        wcClaimantDetails.setDob(insuredDetails.getDob());
        wcClaimantDetails.setAge(insuredDetails.getAge());
        wcClaimantDetails.setSex(insuredDetails.getSex());
        wcClaimantDetails.setPolicyCategory(insuredDetails.getPolicyCategory());
        wcClaimantDetails.setMaritalstatus(insuredDetails.getMaritalstatus());
        wcClaimantDetails.setOccupation(insuredDetails.getOccupation());
        wcClaimantDetails.setEmployementStatus(employeeDetails.getEmployementStatus());
        wcClaimantDetails.setFeinNumber(insuredDetails.getFeinNumber());
        wcClaimantDetails.setPrimaryPhone(insuredDetails.getPrimaryPhone());
        wcClaimantDetails.setSecondaryPhone(insuredDetails.getSecondaryPhone());
        wcClaimantDetails.setPrimaryEmail(insuredDetails.getPrimaryEmail());
        wcClaimantDetails.setSecondaryEmail(insuredDetails.getSecondaryEmail());
        wcClaimantDetails.setNumOfDependents(insuredDetails.getNumOfDependents());
        wcClaimantDetails.setEmployeeCode(employeeDetails.getEmployeeCode());
        wcClaimantDetails.setSSN_Number(employeeDetails.getSSN_Number());
        wcClaimantDetails.setDateHired(employeeDetails.getDateHired());
        wcClaimantDetails.setStateHired(employeeDetails.getStateHired());
        wcClaimantDetails.setNcciCodeID(employeeDetails.getNcciCodeID());
        wcClaimantDetails.setWageFrequencyID(employeeDetails.getWageFrequencyID());
        wcClaimantDetails.setAvgWeeklyWages(employeeDetails.getAvgWeeklyWages());
        wcClaimantDetails.setNoOfDaysPerWeek(employeeDetails.getNoOfDaysPerWeek());
        wcClaimantDetails.setFullPayDayOfInjury(employeeDetails.getFullPayDayOfInjury());
        wcClaimantDetails.setSalaryStatus(employeeDetails.getSalaryStatus());
        wcClaimantDetails.setWhereTaken(insuredDetails.getWhereTaken());
        wcClaimantDetails.setStatus("Active");
        wcClaimantDetails.setModifiedBy(insuredDetails.getPrimaryEmail());
        wcClaimantDetails.setModifiedOn(LocalDateTime.now());
        repository.save(wcClaimantDetails);
    }

    @Override
    public void map(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasWcClaimantDetails()) {
                Optional<WCClaimantDetails> wcClaimantDetails = repository.findTopByClaimIDOrderByModifiedOnDesc(utilityDTO.getClaimId());
                wcClaimantDetails.ifPresent(claimsList::setWcClaimantDetails);
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        witnessDetailsService.map(claimsList, utilityDTO);
    }

    @Override
    public void update(ClaimsList claimsList, UtilityDTO utilityDTO) {
        WCClaimantDetails wcClaimantDetails=claimsList.getWcClaimantDetails();
        if(wcClaimantDetails!=null){
            saveToDB(utilityDTO,wcClaimantDetails);
        }
        // WC claiment details update is not required to update so directly passed to next flow #WitnessDetails
        witnessDetailsService.update(claimsList, utilityDTO);
    }
}
