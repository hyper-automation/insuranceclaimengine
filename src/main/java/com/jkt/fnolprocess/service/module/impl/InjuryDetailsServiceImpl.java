package com.jkt.fnolprocess.service.module.impl;

import com.jkt.fnolprocess.domains.ClaimsList;
import com.jkt.fnolprocess.domains.IncidentDetailsPojo;
import com.jkt.fnolprocess.domains.InjuryDetailsPojo;
import com.jkt.fnolprocess.entities.InjuryDetails;
import com.jkt.fnolprocess.entities.TreatmentDetails;
import com.jkt.fnolprocess.repositories.InjuryDetailsRepository;
import com.jkt.fnolprocess.service.module.InjuryDetailsService;
import com.jkt.fnolprocess.service.module.PropertyDetailsService;
import com.jkt.fnolprocess.service.module.TreatmentDetailsService;
import com.jkt.fnolprocess.util.ModuleStatus;
import com.jkt.fnolprocess.util.UtilityDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class InjuryDetailsServiceImpl implements InjuryDetailsService {

    @Autowired
    InjuryDetailsRepository repository;

    @Autowired
    PropertyDetailsService propertyDetailsService;

    @Autowired
    TreatmentDetailsService treatmentDetailsService;

    @Override
    public void save(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasInjuryDetails() && claimsList.getInjuryDetails() != null) {
                for (InjuryDetailsPojo injuryDetailsPojo : claimsList.getInjuryDetails()) {
                    saveToDB(utilityDTO, injuryDetailsPojo);
                }
                utilityDTO.add(ModuleStatus.success("Injury details saved"));
            }
        }catch (Exception e){
            log.error(e.getMessage(), e);
            utilityDTO.add(ModuleStatus.failed("Injury details save failed :: " + e.getMessage()));
        }
        propertyDetailsService.save(claimsList, utilityDTO);
    }

    private void saveToDB(UtilityDTO utilityDTO, InjuryDetailsPojo injuryDetailsPojo) {
        InjuryDetails injuryDetails = InjuryDetails.getEntity(injuryDetailsPojo);
        injuryDetails.setModifiedBy(utilityDTO.getEmail());
        injuryDetails.setClaimID(utilityDTO.getClaimId());
        injuryDetails = repository.save(injuryDetails);
        if(injuryDetailsPojo.getTreatmentDetails() != null && !injuryDetailsPojo.getTreatmentDetails().isEmpty()) {
            treatmentDetailsService.save(injuryDetailsPojo.getTreatmentDetails(), injuryDetails.getInjuryID(), utilityDTO);
        }
    }


    @Override
    public void map(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasInjuryDetails()) {
                List<InjuryDetails> injuryDetailsList = repository.findByClaimID(utilityDTO.getClaimId());
                List<InjuryDetailsPojo> injuryDetailsPojos = injuryDetailsList.stream()
                        .map(InjuryDetails::toPojo)
                        .map(pojo -> {
                            List<TreatmentDetails> treatmentDetails = treatmentDetailsService.get(pojo.getInjuryID());
                            pojo.setTreatmentDetails(treatmentDetails);
                            return pojo;
                        })
                        .collect(Collectors.toList());
                claimsList.setInjuryDetails(injuryDetailsPojos);
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        propertyDetailsService.map(claimsList, utilityDTO);
    }

    @Override
    public void update(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasInjuryDetails() && claimsList.getInjuryDetails() != null && !claimsList.getInjuryDetails().isEmpty()) {
                for (InjuryDetailsPojo injuryDetails : claimsList.getInjuryDetails()) {
                    if (injuryDetails.getInjuryID() != null) {
                        InjuryDetails injurydDetailsDb = repository.findByClaimIDAndInjuryID(utilityDTO.getClaimId(), injuryDetails.getInjuryID()).get();
                        injurydDetailsDb.setInjuryDate((injuryDetails.getInjuryDate() == null) ? injurydDetailsDb.getInjuryDate() : injuryDetails.getInjuryDate());
                        injurydDetailsDb.setInjuryCause((injuryDetails.getInjuryCause() == null) ? injurydDetailsDb.getInjuryCause() : injuryDetails.getInjuryCause());
                        injurydDetailsDb.setInjuryType((injuryDetails.getInjuryType() == null) ? injurydDetailsDb.getInjuryType() : injuryDetails.getInjuryType());
                        injurydDetailsDb.setBodyCode((injuryDetails.getBodyCode() == null) ? injurydDetailsDb.getBodyCode() : injuryDetails.getBodyCode());
                        injurydDetailsDb.setFatalStatus(StringUtils.isBlank(injuryDetails.getFatalStatus()) ? injurydDetailsDb.getFatalStatus() : injuryDetails.getFatalStatus());
                        injurydDetailsDb.setDeathDate((injuryDetails.getDeathDate() == null) ? injurydDetailsDb.getDeathDate() : injuryDetails.getDeathDate());
                        injurydDetailsDb.setBeginWork(StringUtils.isBlank(injuryDetails.getBeginWork()) ? injurydDetailsDb.getBeginWork() : injuryDetails.getBeginWork());
                        injurydDetailsDb.setReturnDate((injuryDetails.getReturnDate() == null) ? injurydDetailsDb.getReturnDate() : injuryDetails.getReturnDate());
                        injurydDetailsDb.setDisabilityBeganDate((injuryDetails.getDisabilityBeganDate() == null) ? injurydDetailsDb.getDisabilityBeganDate() : injuryDetails.getDisabilityBeganDate());
                        injurydDetailsDb.setModifiedOn(LocalDateTime.now());
                        injurydDetailsDb.setModifiedBy(utilityDTO.getEmail());
                        repository.save(injurydDetailsDb);
                        if(injuryDetails.getTreatmentDetails() != null && !injuryDetails.getTreatmentDetails().isEmpty()){
                            treatmentDetailsService.update(injuryDetails.getTreatmentDetails(), injuryDetails.getInjuryID(), utilityDTO);
                        }
                    } else
                        saveToDB(utilityDTO, injuryDetails);
                }
            }
            utilityDTO.add(ModuleStatus.success("Injury details updated"));
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("Injury details update failed :: " + e.getMessage()));
        }
        propertyDetailsService.update(claimsList, utilityDTO);
    }
}