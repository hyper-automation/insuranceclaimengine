package com.jkt.fnolprocess.service.module.impl;

import com.jkt.fnolprocess.domains.ClaimsList;
import com.jkt.fnolprocess.domains.IncidentDetailsPojo;
import com.jkt.fnolprocess.entities.GLOccuranceDetails;
import com.jkt.fnolprocess.repositories.GLOccuranceDetailsRepository;
import com.jkt.fnolprocess.service.module.GlOccuranceDetailsService;
import com.jkt.fnolprocess.util.ModuleStatus;
import com.jkt.fnolprocess.util.UtilityDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Slf4j
public class GlOccuranceDetailsServiceImpl implements GlOccuranceDetailsService {
    @Autowired
    GLOccuranceDetailsRepository repository;

    @Autowired
    ServiceHelper helper;

    @Override
    public void save(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasGLOccuranceDetails() && claimsList.getGlOccuranceDetails() != null) {
                saveToDb(claimsList, utilityDTO);
              utilityDTO.add(ModuleStatus.success("GL Occurrence details saved"));
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("GL Occurrence details save failed :: " + e.getMessage()));
        }
    }

    private void saveToDb(ClaimsList claimsList, UtilityDTO utilityDTO) {
        GLOccuranceDetails glOccuranceDetails = claimsList.getGlOccuranceDetails();
        glOccuranceDetails.setClaimID(utilityDTO.getClaimId());
        glOccuranceDetails.setStatus("Active");
        glOccuranceDetails.setModifiedBy(utilityDTO.getEmail());
        glOccuranceDetails.setModifiedOn(LocalDateTime.now());
        repository.save(glOccuranceDetails);
    }

    @Override
    public void map(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasGLOccuranceDetails()) {
                Optional<GLOccuranceDetails> glOccuranceDetails = repository.findTopByClaimIDOrderByModifiedOnDesc(utilityDTO.getClaimId());
                glOccuranceDetails.ifPresent(claimsList::setGlOccuranceDetails);
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
    }

    @Override
    public void update(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasGLOccuranceDetails() && claimsList.getGlOccuranceDetails() != null) {

                Optional<GLOccuranceDetails> glOccuranceDetailsDbOptional = repository.findTopByClaimIDOrderByModifiedOnDesc(utilityDTO.getClaimId());
                if (glOccuranceDetailsDbOptional.isPresent()) {
                    GLOccuranceDetails glOccuranceDetailsNew = claimsList.getGlOccuranceDetails();
                    GLOccuranceDetails glOccuranceDetailsDb = glOccuranceDetailsDbOptional.get();
                    glOccuranceDetailsNew.setOccuranceID(null); // Always save new record for GL Occurrence
                    glOccuranceDetailsNew.setClaimID(utilityDTO.getClaimId());
                    glOccuranceDetailsNew.setPoliceContacted((glOccuranceDetailsNew.getPoliceContacted() == null) ? glOccuranceDetailsDb.getPoliceContacted() : glOccuranceDetailsNew.getPoliceContacted());
                    glOccuranceDetailsNew.setReportNumber(StringUtils.isBlank(glOccuranceDetailsNew.getReportNumber()) ? glOccuranceDetailsDb.getReportNumber() : glOccuranceDetailsNew.getReportNumber());
                    glOccuranceDetailsNew.setOccurrenceDescription(StringUtils.isBlank(glOccuranceDetailsNew.getOccurrenceDescription()) ? glOccuranceDetailsDb.getOccurrenceDescription() : glOccuranceDetailsNew.getOccurrenceDescription());
                    glOccuranceDetailsNew.setAddress((glOccuranceDetailsNew.getAddress() != null) ? helper.updateAddress(glOccuranceDetailsNew.getAddress()) : glOccuranceDetailsDb.getAddress());
                    glOccuranceDetailsNew.setModifiedOn(LocalDateTime.now());
                    glOccuranceDetailsNew.setModifiedBy(utilityDTO.getEmail());
                    repository.save(glOccuranceDetailsNew);
                } else {
                    saveToDb(claimsList, utilityDTO);
                }
                utilityDTO.add(ModuleStatus.success("GL Occurrence details updated"));
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("GL Occurrence details update failed :: " + e.getMessage()));
        }
    }
}
