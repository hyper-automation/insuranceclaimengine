package com.jkt.fnolprocess.service.module.impl;

import com.jkt.fnolprocess.domains.ClaimsList;
import com.jkt.fnolprocess.domains.IncidentDetailsPojo;
import com.jkt.fnolprocess.entities.PropertyDetails;
import com.jkt.fnolprocess.repositories.PropertyDetailsRepository;
import com.jkt.fnolprocess.service.module.PropertyDetailsService;
import com.jkt.fnolprocess.service.module.WcClaimantDetailsService;
import com.jkt.fnolprocess.util.ModuleStatus;
import com.jkt.fnolprocess.util.UtilityDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Slf4j
public class PropertyDetailsServiceImpl implements PropertyDetailsService {

    @Autowired
    PropertyDetailsRepository repository;

    @Autowired
    WcClaimantDetailsService wcClaimantDetailsService;

    @Autowired
    ServiceHelper helper;

    @Override
    public void save(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasPropertyDetails() && claimsList.getPropertyDetails() != null) {
                saveToDB(claimsList, utilityDTO);
                utilityDTO.add(ModuleStatus.success("Property details saved"));

            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("Property details save failed :: " + e.getMessage()));
        }
        wcClaimantDetailsService.save(claimsList, utilityDTO);
    }

    private void saveToDB(ClaimsList claimsList, UtilityDTO utilityDTO) {
        PropertyDetails propertyDetails = claimsList.getPropertyDetails();
        propertyDetails.setClaimID(utilityDTO.getClaimId());
        propertyDetails.setStatus("Active");
        propertyDetails.setModifiedBy(utilityDTO.getEmail());
        propertyDetails.setModifiedOn(LocalDateTime.now());
        repository.save(propertyDetails);
    }

    @Override
    public void map(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasPropertyDetails()) {
                Optional<PropertyDetails> propertyDetails = repository.findByClaimID(utilityDTO.getClaimId());
                propertyDetails.ifPresent(claimsList::setPropertyDetails);
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        wcClaimantDetailsService.map(claimsList, utilityDTO);
    }

    @Override
    public void update(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasPropertyDetails() && claimsList.getPropertyDetails() != null) {
                Optional<PropertyDetails> propertyDetailsDbOptional = repository.findByClaimID(utilityDTO.getClaimId());
                if (propertyDetailsDbOptional.isPresent()) {
                    PropertyDetails propertyDetailsDb = propertyDetailsDbOptional.get();
                    PropertyDetails propertyDetailsNew = claimsList.getPropertyDetails();
                    propertyDetailsDb.setFirstName(StringUtils.isBlank(propertyDetailsNew.getFirstName()) ? propertyDetailsDb.getFirstName() : propertyDetailsNew.getFirstName());
                    propertyDetailsDb.setLastName(StringUtils.isBlank(propertyDetailsNew.getLastName()) ? propertyDetailsDb.getLastName() : propertyDetailsNew.getLastName());
                    propertyDetailsDb.setDob((propertyDetailsNew.getDob() == null) ? propertyDetailsDb.getDob() : propertyDetailsNew.getDob());
                    propertyDetailsDb.setAge((propertyDetailsNew.getAge() == null) ? propertyDetailsDb.getAge() : propertyDetailsNew.getAge());
                    propertyDetailsDb.setSex(StringUtils.isBlank(propertyDetailsNew.getSex()) ? propertyDetailsDb.getSex() : propertyDetailsNew.getSex());
                    propertyDetailsDb.setDescribeProperty(StringUtils.isBlank(propertyDetailsNew.getDescribeProperty()) ? propertyDetailsDb.getDescribeProperty() : propertyDetailsNew.getDescribeProperty());
                    propertyDetailsDb.setPrimaryPhone(StringUtils.isBlank(propertyDetailsNew.getPrimaryPhone()) ? propertyDetailsDb.getPrimaryPhone() : propertyDetailsNew.getPrimaryPhone());
                    propertyDetailsDb.setPrimaryPhoneType(StringUtils.isBlank(propertyDetailsNew.getPrimaryPhoneType()) ? propertyDetailsDb.getPrimaryPhoneType() : propertyDetailsNew.getPrimaryPhoneType());
                    propertyDetailsDb.setSecondaryPhone(StringUtils.isBlank(propertyDetailsNew.getSecondaryPhone()) ? propertyDetailsDb.getSecondaryPhone() : propertyDetailsNew.getSecondaryPhone());
                    propertyDetailsDb.setSecondaryPhoneType(StringUtils.isBlank(propertyDetailsNew.getSecondaryPhoneType()) ? propertyDetailsDb.getSecondaryPhoneType() : propertyDetailsNew.getSecondaryPhoneType());
                    propertyDetailsDb.setPrimaryEmail(StringUtils.isBlank(propertyDetailsNew.getPrimaryEmail()) ? propertyDetailsDb.getPrimaryEmail() : propertyDetailsNew.getPrimaryEmail());
                    propertyDetailsDb.setSecondaryEmail(StringUtils.isBlank(propertyDetailsNew.getSecondaryEmail()) ? propertyDetailsDb.getSecondaryEmail() : propertyDetailsNew.getSecondaryEmail());
                    propertyDetailsDb.setEstimateAmount((propertyDetailsNew.getEstimateAmount() == null) ? propertyDetailsDb.getEstimateAmount() : propertyDetailsNew.getEstimateAmount());
                    propertyDetailsDb.setWherePropertyCanBeSeen(StringUtils.isBlank(propertyDetailsNew.getWherePropertyCanBeSeen()) ? propertyDetailsDb.getWherePropertyCanBeSeen() : propertyDetailsNew.getWherePropertyCanBeSeen());
                    propertyDetailsDb.setAddress((propertyDetailsNew.getAddress() != null) ? helper.updateAddress(propertyDetailsNew.getAddress()) : propertyDetailsDb.getAddress());
                    propertyDetailsDb.setModifiedOn(LocalDateTime.now());
                    propertyDetailsDb.setModifiedBy(utilityDTO.getEmail());
                    repository.save(propertyDetailsDb);
                } else {
                    saveToDB(claimsList, utilityDTO);
                }
            }
            utilityDTO.add(ModuleStatus.success("Property details updated"));
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("Property details update failed :: " + e.getMessage()));
        }
        wcClaimantDetailsService.update(claimsList, utilityDTO);
    }
}
