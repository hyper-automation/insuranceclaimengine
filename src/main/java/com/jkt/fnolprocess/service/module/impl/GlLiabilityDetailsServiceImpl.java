package com.jkt.fnolprocess.service.module.impl;

import com.jkt.fnolprocess.domains.ClaimsList;
import com.jkt.fnolprocess.entities.GLLiabilityDetails;
import com.jkt.fnolprocess.entities.ManufacturerDetails;
import com.jkt.fnolprocess.entities.VendorDetails;
import com.jkt.fnolprocess.repositories.GLLiabilityDetailsRepository;
import com.jkt.fnolprocess.service.module.GlLiabilityDetailsService;
import com.jkt.fnolprocess.service.module.GlOccuranceDetailsService;
import com.jkt.fnolprocess.util.ModuleStatus;
import com.jkt.fnolprocess.util.UtilityDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Slf4j
public class GlLiabilityDetailsServiceImpl implements GlLiabilityDetailsService {

    @Autowired
    GLLiabilityDetailsRepository repository;

    @Autowired
    ServiceHelper helper;

    @Autowired
    GlOccuranceDetailsService glOccuranceDetailsService;

    public void save(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasGlLiabilityDetails() && claimsList.getGlLiabilityDetails() != null) {
                saveToDB(claimsList, utilityDTO);
                utilityDTO.add(ModuleStatus.success("GL Liability details saved"));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            utilityDTO.add(ModuleStatus.failed("GL Liability details save failed :: " + e.getMessage()));
        }
        glOccuranceDetailsService.save(claimsList, utilityDTO);
    }

    private void saveToDB(ClaimsList claimsList, UtilityDTO utilityDTO) {
        GLLiabilityDetails glLiabilityDetails = claimsList.getGlLiabilityDetails();
        if (glLiabilityDetails.getVendor() != null) {
            VendorDetails vendorDetails = glLiabilityDetails.getVendor();
            vendorDetails.setStatus("Active");
            vendorDetails.setModifiedBy(utilityDTO.getEmail());
            vendorDetails.setModifiedOn(LocalDateTime.now());
            glLiabilityDetails.setVendor(vendorDetails);
        }
        if (glLiabilityDetails.getManufactuter() != null) {
            ManufacturerDetails manufacturerDetails = glLiabilityDetails.getManufactuter();
            manufacturerDetails.setStatus("Active");
            manufacturerDetails.setModifiedBy(utilityDTO.getEmail());
            manufacturerDetails.setModifiedOn(LocalDateTime.now());
            glLiabilityDetails.setManufactuter(manufacturerDetails);
        }
        glLiabilityDetails.setClaimID(utilityDTO.getClaimId());
        glLiabilityDetails.setStatus("Active");
        glLiabilityDetails.setModifiedBy(utilityDTO.getEmail());
        glLiabilityDetails.setModifiedOn(LocalDateTime.now());
        repository.save(glLiabilityDetails);
    }

    @Override
    public void map(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasGlLiabilityDetails()) {
                Optional<GLLiabilityDetails> glLiabilityDetails = repository.findByClaimID(utilityDTO.getClaimId());
                glLiabilityDetails.ifPresent(claimsList::setGlLiabilityDetails);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        glOccuranceDetailsService.map(claimsList, utilityDTO);
    }

    @Override
    public void update(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasGlLiabilityDetails() && claimsList.getGlLiabilityDetails() != null) {
                Optional<GLLiabilityDetails> glLiabilityDetailsDbOptional = repository.findByClaimID(utilityDTO.getClaimId());
                if (glLiabilityDetailsDbOptional.isPresent()) {
                    GLLiabilityDetails glLiabilityDetailsDb = glLiabilityDetailsDbOptional.get();
                    GLLiabilityDetails glLiabilityDetailsNew = claimsList.getGlLiabilityDetails();
                    glLiabilityDetailsDb.setPremisesInsured(StringUtils.isBlank(glLiabilityDetailsNew.getPremisesInsured()) ? glLiabilityDetailsDb.getPremisesInsured() : glLiabilityDetailsNew.getPremisesInsured());
                    glLiabilityDetailsDb.setProductInsured(StringUtils.isBlank(glLiabilityDetailsNew.getProductInsured()) ? glLiabilityDetailsDb.getProductInsured() : glLiabilityDetailsNew.getProductInsured());
                    glLiabilityDetailsDb.setTypeOfPremises(StringUtils.isBlank(glLiabilityDetailsNew.getTypeOfPremises()) ? glLiabilityDetailsDb.getTypeOfPremises() : glLiabilityDetailsNew.getTypeOfPremises());
                    //   glLiabilityDetailsDb.setManufactuter((glLiabilityDetailsNew.getManufactuter() != null) ? helper.updateManufacturer(glLiabilityDetailsNew.getManufactuter()) : glLiabilityDetailsDb.getManufactuter());
                    // glLiabilityDetailsDb.setVendor((glLiabilityDetailsNew.getVendor() != null) ? helper.updateVendor(glLiabilityDetailsNew.getVendor()) : glLiabilityDetailsDb.getVendor());
                    glLiabilityDetailsDb.setModifiedOn(LocalDateTime.now());
                    glLiabilityDetailsDb.setModifiedBy(utilityDTO.getEmail());
                } else {
                    saveToDB(claimsList, utilityDTO);
                }
            }
            utilityDTO.add(ModuleStatus.success("GL Liability details updated"));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            utilityDTO.add(ModuleStatus.failed("GL Liability details update failed :: " + e.getMessage()));
        }
        glOccuranceDetailsService.update(claimsList, utilityDTO);
    }
}
