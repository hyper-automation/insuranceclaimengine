package com.jkt.fnolprocess.service.module;

import com.jkt.fnolprocess.entities.TreatmentDetails;
import com.jkt.fnolprocess.util.UtilityDTO;

import java.util.List;

public interface TreatmentDetailsService {
    void save(List<TreatmentDetails> treatmentDetails, Integer injuryID, UtilityDTO utilityDTO);

    List<TreatmentDetails> get(Integer injuryId);

    void update(List<TreatmentDetails> treatmentDetails, Integer injuryID, UtilityDTO utilityDTO);
}
