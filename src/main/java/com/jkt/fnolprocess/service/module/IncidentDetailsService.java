package com.jkt.fnolprocess.service.module;

import com.jkt.fnolprocess.domains.IncidentDetailsPojo;
import com.jkt.fnolprocess.entities.IncidentDetails;
import com.jkt.fnolprocess.util.UtilityDTO;

import java.util.List;

public interface IncidentDetailsService {
    void save(IncidentDetailsPojo incidentDetailsPojo, UtilityDTO utilityDTO);

    void update(IncidentDetailsPojo incidentDetailsPojo, UtilityDTO utilityDTO);

    List<IncidentDetails> getAllIncidentDetails();

    void map(IncidentDetailsPojo incidentDetailsPojo, UtilityDTO utilityDTO);

}
