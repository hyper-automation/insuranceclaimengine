package com.jkt.fnolprocess.service.module.impl;

import com.jkt.fnolprocess.entities.TreatmentDetails;
import com.jkt.fnolprocess.repositories.TreatmentDetailsRepository;
import com.jkt.fnolprocess.service.module.TreatmentDetailsService;
import com.jkt.fnolprocess.util.ModuleStatus;
import com.jkt.fnolprocess.util.UtilityDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TreatmentDetailsServiceImpl implements TreatmentDetailsService {

    @Autowired
    TreatmentDetailsRepository repository;

    @Autowired
    ServiceHelper helper;

    @Override
    public void save(List<TreatmentDetails> treatmentDetailsList, Integer injuryID, UtilityDTO utilityDTO) {
        try {
            List<TreatmentDetails> treatmentDetailsUpdated = treatmentDetailsList.stream().filter(Objects::nonNull)
                    .map(treatmentDetails -> {
                        treatmentDetails.setInjuryID(injuryID);
                        treatmentDetails.setStatus("Active");
                        treatmentDetails.setModifiedBy(utilityDTO.getEmail());
                        treatmentDetails.setModifiedOn(LocalDateTime.now());
                        return treatmentDetails;
                    }).collect(Collectors.toList());
            repository.saveAll(treatmentDetailsUpdated);
            utilityDTO.add(ModuleStatus.success("Treatment details saved"));
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("Treatment details save failed :: " + e.getMessage()));
        }
    }

    @Override
    public List<TreatmentDetails> get(Integer injuryId) {
        try {
            return repository.findByInjuryID(injuryId);
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        return  new ArrayList<>();
    }

    @Override
    public void update(List<TreatmentDetails> treatmentDetails, Integer injuryID, UtilityDTO utilityDTO) {
        try {
            for (TreatmentDetails treatmentDetailsNew : treatmentDetails) {
                if (treatmentDetailsNew.getTreatmentID() != null) {
                    TreatmentDetails treatmentDetailsDb = repository.findByInjuryIDAndTreatmentID(injuryID, treatmentDetailsNew.getTreatmentID()).get();
                    treatmentDetailsDb.setPhysicianName(StringUtils.isBlank(treatmentDetailsNew.getPhysicianName()) ? treatmentDetailsDb.getPhysicianName() : treatmentDetailsNew.getPhysicianName());
                    treatmentDetailsDb.setInitialTreatment(StringUtils.isBlank(treatmentDetailsNew.getInitialTreatment()) ? treatmentDetailsDb.getInitialTreatment() : treatmentDetailsNew.getInitialTreatment());
                    treatmentDetailsDb.setHospName(StringUtils.isBlank(treatmentDetailsNew.getHospName()) ? treatmentDetailsDb.getHospName() : treatmentDetailsNew.getHospName());
                    treatmentDetailsDb.setAdministratorNotifiedDate((treatmentDetailsNew.getAdministratorNotifiedDate() == null) ? treatmentDetailsDb.getAdministratorNotifiedDate() : treatmentDetailsNew.getAdministratorNotifiedDate());
                    treatmentDetailsDb.setAddress((treatmentDetailsNew.getAddress() != null) ? helper.updateAddress(treatmentDetailsNew.getAddress()) : treatmentDetailsDb.getAddress());
                    treatmentDetailsDb.setModifiedOn(LocalDateTime.now());
                    treatmentDetailsNew.setModifiedBy(utilityDTO.getEmail());
                    repository.save(treatmentDetailsDb);
                } else {
                    treatmentDetailsNew.setInjuryID(injuryID);
                    treatmentDetailsNew.setStatus("Active");
                    treatmentDetailsNew.setModifiedBy(utilityDTO.getEmail());
                    treatmentDetailsNew.setModifiedOn(LocalDateTime.now());
                    repository.save(treatmentDetailsNew);
                }
            }
            utilityDTO.add(ModuleStatus.success("Treatment details updated"));
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("Treatment details update failed :: " + e.getMessage()));
        }

    }
}
