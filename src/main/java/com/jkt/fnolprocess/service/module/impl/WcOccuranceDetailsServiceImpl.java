package com.jkt.fnolprocess.service.module.impl;

import com.jkt.fnolprocess.domains.ClaimsList;
import com.jkt.fnolprocess.domains.IncidentDetailsPojo;
import com.jkt.fnolprocess.entities.WCOccuranceDetails;
import com.jkt.fnolprocess.repositories.WcOccuranceDetailsRepository;
import com.jkt.fnolprocess.service.module.InjuryDetailsService;
import com.jkt.fnolprocess.service.module.WcOccuranceDetailsService;
import com.jkt.fnolprocess.util.ModuleStatus;
import com.jkt.fnolprocess.util.UtilityDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Slf4j
public class WcOccuranceDetailsServiceImpl implements WcOccuranceDetailsService {

    @Autowired
    WcOccuranceDetailsRepository repository;

    @Autowired
    InjuryDetailsService injuryDetailsService;

    @Autowired
    ServiceHelper helper;

    @Override
    public void save(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasWCOccuranceDetails() && claimsList.getWcOccuranceDetails() != null) {
                saveToDB(claimsList, utilityDTO);
                utilityDTO.add(ModuleStatus.success("WC Occurrence details saved"));
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("WC Occurrence details save failed :: " + e.getMessage()));
        }
        injuryDetailsService.save(claimsList, utilityDTO);
    }

    private void saveToDB(ClaimsList claimsList, UtilityDTO utilityDTO) {
        WCOccuranceDetails wcOccuranceDetails = claimsList.getWcOccuranceDetails();
        wcOccuranceDetails.setClaimID(utilityDTO.getClaimId());
        wcOccuranceDetails.setStatus("Active");
        wcOccuranceDetails.setModifiedBy(utilityDTO.getEmail());
        wcOccuranceDetails.setModifiedOn(LocalDateTime.now());
        repository.save(wcOccuranceDetails);
    }

    @Override
    public void map(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasWCOccuranceDetails()) {
                Optional<WCOccuranceDetails> wcOccuranceDetails = repository.findTopByClaimIDOrderByModifiedOnDesc(utilityDTO.getClaimId());
                wcOccuranceDetails.ifPresent(claimsList::setWcOccuranceDetails);
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        injuryDetailsService.map(claimsList, utilityDTO);
    }

    @Override
    public void update(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasWCOccuranceDetails() && claimsList.getWcOccuranceDetails() != null) {
                Optional<WCOccuranceDetails> wcOccuranceDetailsDbOptional = repository.findTopByClaimIDOrderByModifiedOnDesc(utilityDTO.getClaimId());
                if (wcOccuranceDetailsDbOptional.isPresent()) {
                    WCOccuranceDetails wcOccuranceDetailsDb = wcOccuranceDetailsDbOptional.get();
                    WCOccuranceDetails wcOccuranceDetailsNew = claimsList.getWcOccuranceDetails();
                    wcOccuranceDetailsNew.setOccuranceID(null); // TO save the Wc Occurrence always as new record
                    wcOccuranceDetailsNew.setClaimID(utilityDTO.getClaimId());
                    wcOccuranceDetailsNew.setEmployerNotifiedDate((wcOccuranceDetailsNew.getEmployerNotifiedDate() == null) ? wcOccuranceDetailsDb.getEmployerNotifiedDate() : wcOccuranceDetailsNew.getEmployerNotifiedDate());
                    wcOccuranceDetailsNew.setOccurredDepartment(StringUtils.isBlank(wcOccuranceDetailsDb.getOccurredDepartment()) ? wcOccuranceDetailsDb.getOccurredDepartment() : wcOccuranceDetailsNew.getOccurredDepartment());
                    wcOccuranceDetailsNew.setEquipmentList(StringUtils.isBlank(wcOccuranceDetailsDb.getEquipmentList()) ? wcOccuranceDetailsDb.getEquipmentList() : wcOccuranceDetailsNew.getEquipmentList());
                    wcOccuranceDetailsNew.setSpecificActivity(StringUtils.isBlank(wcOccuranceDetailsDb.getSpecificActivity()) ? wcOccuranceDetailsDb.getSpecificActivity() : wcOccuranceDetailsNew.getSpecificActivity());
                    wcOccuranceDetailsNew.setLastWorkDate((wcOccuranceDetailsDb.getLastWorkDate() == null) ? wcOccuranceDetailsDb.getLastWorkDate() : wcOccuranceDetailsNew.getLastWorkDate());
                    wcOccuranceDetailsNew.setDidOccuredOnEmpPremises(StringUtils.isBlank(wcOccuranceDetailsDb.getDidOccuredOnEmpPremises()) ? wcOccuranceDetailsDb.getDidOccuredOnEmpPremises() : wcOccuranceDetailsNew.getDidOccuredOnEmpPremises());
                    wcOccuranceDetailsNew.setOccurrenceDetails(StringUtils.isBlank(wcOccuranceDetailsDb.getOccurrenceDetails()) ? wcOccuranceDetailsDb.getOccurrenceDetails() : wcOccuranceDetailsNew.getOccurrenceDetails());
                    wcOccuranceDetailsNew.setEventSequence(StringUtils.isBlank(wcOccuranceDetailsDb.getEventSequence()) ? wcOccuranceDetailsDb.getEventSequence() : wcOccuranceDetailsNew.getEventSequence());
                    wcOccuranceDetailsNew.setOcurrenceTime((wcOccuranceDetailsDb.getOcurrenceTime() == null) ? wcOccuranceDetailsDb.getOcurrenceTime() : wcOccuranceDetailsNew.getOcurrenceTime());
                    wcOccuranceDetailsNew.setSafetyEqupProvided(StringUtils.isBlank(wcOccuranceDetailsDb.getSafetyEqupProvided()) ? wcOccuranceDetailsDb.getSafetyEqupProvided() : wcOccuranceDetailsNew.getSafetyEqupProvided());
                    wcOccuranceDetailsNew.setSafetyEqupUsed(StringUtils.isBlank(wcOccuranceDetailsDb.getSafetyEqupUsed()) ? wcOccuranceDetailsDb.getSafetyEqupUsed() : wcOccuranceDetailsNew.getSafetyEqupUsed());
                    wcOccuranceDetailsNew.setWorkProcess(StringUtils.isBlank(wcOccuranceDetailsDb.getWorkProcess()) ? wcOccuranceDetailsDb.getWorkProcess() : wcOccuranceDetailsNew.getWorkProcess());
                    wcOccuranceDetailsNew.setAddress((wcOccuranceDetailsDb.getAddress() != null) ? helper.updateAddress(wcOccuranceDetailsDb.getAddress()) : wcOccuranceDetailsDb.getAddress());
                    wcOccuranceDetailsNew.setModifiedOn(LocalDateTime.now());
                    wcOccuranceDetailsNew.setModifiedBy(utilityDTO.getEmail());
                    repository.save(wcOccuranceDetailsNew);
                } else {
                    saveToDB(claimsList, utilityDTO);
                }
            }
            utilityDTO.add(ModuleStatus.success("WC Occurrence details saved"));
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("WC Occurrence details updated failed :: " + e.getMessage()));
        }
        injuryDetailsService.update(claimsList, utilityDTO);
    }
}
