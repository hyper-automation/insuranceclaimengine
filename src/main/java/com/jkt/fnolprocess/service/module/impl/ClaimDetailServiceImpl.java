package com.jkt.fnolprocess.service.module.impl;

import com.jkt.fnolprocess.domains.ClaimsList;
import com.jkt.fnolprocess.entities.ClaimDetails;
import com.jkt.fnolprocess.entities.GLInsuredDetails;
import com.jkt.fnolprocess.entities.InsuredDetails;
import com.jkt.fnolprocess.entities.PolicyDetails;
import com.jkt.fnolprocess.repositories.ClaimDetailsRepository;
import com.jkt.fnolprocess.repositories.GLInsuredDetailsRepository;
import com.jkt.fnolprocess.repositories.InsuredDetailsRepository;
import com.jkt.fnolprocess.repositories.PolicyRepository;
import com.jkt.fnolprocess.service.module.ClaimDetailService;
import com.jkt.fnolprocess.service.module.DocumentDetailsService;
import com.jkt.fnolprocess.util.ClaimSourceTypeEnum;
import com.jkt.fnolprocess.util.ModuleStatus;
import com.jkt.fnolprocess.util.UtilityDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class ClaimDetailServiceImpl implements ClaimDetailService {
    @Autowired
    ClaimDetailsRepository repository;

    @Autowired
    InsuredDetailsRepository insuredDetailsRepository;

    @Autowired
    GLInsuredDetailsRepository glInsuredDetailsRepository;

    @Autowired
    PolicyRepository policyRepository;

    @Autowired
    DocumentDetailsService documentDetailsService;

    @Override
    public void save(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasClaimDetails() && claimsList.getClaimDetails() != null) {
                utilityDTO.setClaimId(utilityDTO.generatorForClaimId());
                if (claimsList.getClaimDetails().getClaimType() == 2) {
                    utilityDTO.setEmail(claimsList.getWcClaimantDetails().getPrimaryEmail());
                } else if (claimsList.getClaimDetails().getClaimType() == 1) {
                    utilityDTO.setEmail(claimsList.getGlClaimantDetails().getPrimaryEmail());
                }
                saveToDB(claimsList, utilityDTO);
                utilityDTO.add(ModuleStatus.success("Claim details saved"));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            utilityDTO.add(ModuleStatus.failed("claim details save failed" + e.getMessage()));
            utilityDTO.exitFlow();
        }
        documentDetailsService.save(claimsList, utilityDTO);
    }

    private void saveToDB(ClaimsList claimsList, UtilityDTO utilityDTO) {
        ClaimDetails claimDetails = claimsList.getClaimDetails();
        claimDetails.setStageTypeID(1);
        claimDetails.setClaimStatus(1);
        claimDetails.setClaimCategoryId(4);
        claimDetails.setModifiedOn(LocalDateTime.now());
        claimDetails.setClaimCreatedOn(LocalDateTime.now());
        claimDetails.setStatus("Active");
        claimDetails.setSubClaimType((claimsList.getClaimDetails().getSubClaimType()==null)?1:claimsList.getClaimDetails().getSubClaimType());
        claimDetails.setEmailClaimId((claimsList.getClaimDetails().getEmailClaimId() == null) ? null : claimsList.getClaimDetails().getEmailClaimId());
        claimDetails.setClaimID(utilityDTO.getClaimId());
        claimDetails.setIncidentID(utilityDTO.getIncidentId());
        claimDetails.setClaimCreatedBy(utilityDTO.getEmail());
        claimDetails.setModifiedBy(utilityDTO.getEmail());
        Optional<PolicyDetails> policyDetails = Optional.empty();
        if (utilityDTO.isHasGlClaimantDetails()) {
            Optional<GLInsuredDetails> glInsuredDetails = glInsuredDetailsRepository.findByPrimaryEmail(utilityDTO.getEmail());
            if (glInsuredDetails.isPresent()) {
                policyDetails = policyRepository.findByEmployer(glInsuredDetails.get().getEmployerID());
            }
        } else {
            Optional<InsuredDetails> insuredDetails = insuredDetailsRepository.findByPrimaryEmail(utilityDTO.getEmail());
            if (insuredDetails.isPresent())
                policyDetails = policyRepository.findByEmployer(insuredDetails.get().getEmployerId());
        }
        if (policyDetails.isPresent())
            claimDetails.setPolicyNumber(policyDetails.get().getPolicyNumber());
        claimDetails = saveToDB(claimDetails);
        claimsList.getClaimDetails().setClaimID(claimDetails.getClaimID());
    }

    @Override
    public void map(List<ClaimsList> claimsLists, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasClaimDetails()) {
                List<ClaimDetails> claimDetailsList = repository.findByIncidentID(utilityDTO.getIncidentId());
                if (!claimDetailsList.isEmpty()) {
                    claimDetailsList.forEach(claimsList -> {
                                ClaimsList claim = new ClaimsList();
                                claim.setClaimDetails(claimsList);
                                utilityDTO.setClaimId(claimsList.getClaimID());
                                if (claimsList.getClaimType() == 2) {
                                    utilityDTO.createWCDefault(ClaimSourceTypeEnum.DEFAULT);
                                } else if (claimsList.getClaimType() == 1) {
                                    utilityDTO.createGLDefault(ClaimSourceTypeEnum.DEFAULT);
                                }
                                documentDetailsService.map(claim, utilityDTO);
                                claimsLists.add(claim);
                            }
                    );

                }
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void update(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            utilityDTO.setClaimId(claimsList.getClaimDetails().getClaimID());
            if (utilityDTO.isHasClaimDetails() && claimsList.getClaimDetails() != null) {
                Optional<ClaimDetails> claimDetailsDB = repository.findById(utilityDTO.getClaimId());
                if (claimDetailsDB.isPresent()) {
                    ClaimDetails claimDetails = claimDetailsDB.get();
                    boolean updated = false;
                    if (claimsList.getClaimDetails().getClaimStatus() != null) {
                        claimDetails.setClaimStatus(claimsList.getClaimDetails().getClaimStatus());
                        claimDetails.setStageTypeID(4);
                        updated = true;
                    }
                    if (claimsList.getClaimDetails().getClaimSourceType() != null) {
                        claimDetails.setClaimSourceType(claimsList.getClaimDetails().getClaimSourceType());
                        updated = true;
                    }
                    if (claimsList.getClaimDetails().getStatus() != null) {
                        claimDetails.setStatus(claimsList.getClaimDetails().getStatus());
                        updated = true;
                    }
                    if (updated) {
                        claimDetails.setModifiedOn(LocalDateTime.now());
                        claimDetails.setModifiedBy(utilityDTO.getEmail());
                        repository.save(claimDetails);
                    }
                } else {
                    saveToDB(claimsList, utilityDTO);
                }
            }
            utilityDTO.add(ModuleStatus.success("Claim details updated"));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            utilityDTO.add(ModuleStatus.failed("claim details update failed" + e.getMessage()));
        }
        documentDetailsService.update(claimsList, utilityDTO);

    }

    private ClaimDetails saveToDB(ClaimDetails claimDetails) {
        return repository.save(claimDetails);
    }

    @Override
    public ClaimDetails findByClaimId(String claimId) {
        return repository.findById(claimId).orElse(null);
    }


}
