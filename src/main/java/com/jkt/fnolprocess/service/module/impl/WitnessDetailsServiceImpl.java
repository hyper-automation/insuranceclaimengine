package com.jkt.fnolprocess.service.module.impl;

import com.jkt.fnolprocess.domains.ClaimsList;
import com.jkt.fnolprocess.domains.IncidentDetailsPojo;
import com.jkt.fnolprocess.entities.WitnessDetails;
import com.jkt.fnolprocess.repositories.WitnessDetailsRepository;
import com.jkt.fnolprocess.service.module.GlClaimantDetailsService;
import com.jkt.fnolprocess.service.module.WitnessDetailsService;
import com.jkt.fnolprocess.util.ModuleStatus;
import com.jkt.fnolprocess.util.UtilityDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
public class WitnessDetailsServiceImpl implements WitnessDetailsService {

    @Autowired
    WitnessDetailsRepository repository;

    @Autowired
    GlClaimantDetailsService glClaimantDetailsService;

    @Autowired
    ServiceHelper helper;

    @Override
    public void save(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasWitnessDetails() && claimsList.getWitnessDetails() != null && claimsList.getWitnessDetails().isEmpty()) {

                List<WitnessDetails> witnessDetailsList = claimsList.getWitnessDetails().stream()
                        .filter(Objects::nonNull)
                        .map(witnessDetails -> {
                            witnessDetails.setClaimID(utilityDTO.getClaimId());
                            witnessDetails.setStatus("Active");
                            witnessDetails.setModifiedBy(utilityDTO.getEmail());
                            witnessDetails.setModifiedOn(LocalDateTime.now());
                            return witnessDetails;
                        }).collect(Collectors.toList());
                repository.saveAll(witnessDetailsList);
                utilityDTO.add(ModuleStatus.success("witness Claimant details saved"));
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("Witness details save failed :: " + e.getMessage()));
        }
        glClaimantDetailsService.save(claimsList, utilityDTO);
    }

    @Override
    public void map(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasWitnessDetails()) {
                List<WitnessDetails> witnessDetailsList = repository.findByClaimID(utilityDTO.getClaimId());
                claimsList.setWitnessDetails(witnessDetailsList);
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        glClaimantDetailsService.map(claimsList, utilityDTO);
    }

    @Override
    public void update(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasWitnessDetails() && claimsList.getWitnessDetails() !=null && claimsList.getWitnessDetails().isEmpty()) {
                for (WitnessDetails witness : claimsList.getWitnessDetails()) {
                    if (witness.getWitnessID() != null) {
                        WitnessDetails witnessDetailsDb = repository.findById(witness.getWitnessID()).get();
                        witnessDetailsDb.setWitnessName(StringUtils.isBlank(witness.getWitnessName()) ? witnessDetailsDb.getWitnessName() : witness.getWitnessName());
                        witnessDetailsDb.setPhone(StringUtils.isBlank(witness.getPhone()) ? witnessDetailsDb.getPhone() : witness.getPhone());
                        witnessDetailsDb.setEmail(StringUtils.isBlank(witness.getEmail()) ? witnessDetailsDb.getEmail() : witness.getEmail());
                        witnessDetailsDb.setAddress((witness.getAddress() == null) ? witnessDetailsDb.getAddress() : helper.updateAddress(witness.getAddress()));
                        witnessDetailsDb.setModifiedOn(LocalDateTime.now());
                        witnessDetailsDb.setModifiedBy(utilityDTO.getEmail());
                        repository.save(witnessDetailsDb);
                    } else {
                        witness.setClaimID(utilityDTO.getClaimId());
                        witness.setStatus("Active");
                        witness.setModifiedBy(utilityDTO.getEmail());
                        witness.setModifiedOn(LocalDateTime.now());
                        repository.save(witness);
                    }
                }
            }
            utilityDTO.add(ModuleStatus.success("WC Claimant details updated"));
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("Witness details update failed :: " + e.getMessage()));
        }
        glClaimantDetailsService.update(claimsList, utilityDTO);
    }
}
