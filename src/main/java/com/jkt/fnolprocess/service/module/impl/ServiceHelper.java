package com.jkt.fnolprocess.service.module.impl;

import com.jkt.fnolprocess.entities.AddressDetails;
import com.jkt.fnolprocess.entities.ManufacturerDetails;
import com.jkt.fnolprocess.entities.VendorDetails;
import com.jkt.fnolprocess.repositories.AddressDetailsRepository;
import com.jkt.fnolprocess.repositories.ManufacturerDetailsRepository;
import com.jkt.fnolprocess.repositories.VendorDetailsRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


@Service
@Slf4j
public class ServiceHelper {
    @Autowired
    AddressDetailsRepository addressDetailsRepository;
    @Autowired
    VendorDetailsRepository vendorDetailsRepository;
    @Autowired
    ManufacturerDetailsRepository manufacturerDetailsRepository;

    AddressDetails updateAddress(AddressDetails addressDetails) {
        AddressDetails addressDetailsDb;
        try {
            addressDetailsDb = addressDetailsRepository.findById(addressDetails.getAddressID()).get();
            addressDetailsDb.setAddressName(StringUtils.isBlank(addressDetails.getAddressName()) ? addressDetailsDb.getAddressName() : addressDetails.getAddressName());
            addressDetailsDb.setAddressCity(StringUtils.isBlank(addressDetails.getAddressCity()) ? addressDetailsDb.getAddressCity() : addressDetails.getAddressCity());
            addressDetailsDb.setAddressState((addressDetails.getAddressState() == null) ? addressDetailsDb.getAddressState() : addressDetails.getAddressState());
            addressDetailsDb.setAddressCountry((addressDetails.getAddressCountry() == null) ? addressDetailsDb.getAddressCountry() : addressDetails.getAddressCountry());
            addressDetailsDb.setAddressZip(StringUtils.isBlank(addressDetails.getAddressZip()) ? addressDetailsDb.getAddressZip() : addressDetails.getAddressZip());
            addressDetailsDb = addressDetailsRepository.save(addressDetailsDb);
            return addressDetailsDb;
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        return new AddressDetails();
    }

    ManufacturerDetails updateManufacturer(ManufacturerDetails manufacturerDetails) {
        ManufacturerDetails manufacturerDetailsDb;
        try {
            manufacturerDetailsDb = manufacturerDetailsRepository.findById(manufacturerDetails.getManufacturerID()).get();
            manufacturerDetailsDb.setManufacturerName(StringUtils.isBlank(manufacturerDetails.getManufacturerName()) ? manufacturerDetailsDb.getManufacturerName() : manufacturerDetails.getManufacturerName());
            manufacturerDetailsDb.setPhone(StringUtils.isBlank(manufacturerDetails.getPhone()) ? manufacturerDetailsDb.getPhone() : manufacturerDetails.getPhone());
            manufacturerDetailsDb.setEmail(StringUtils.isBlank(manufacturerDetails.getEmail()) ? manufacturerDetailsDb.getEmail() : manufacturerDetails.getEmail());
            manufacturerDetailsDb.setProductType((manufacturerDetails.getProductType() == null) ? manufacturerDetailsDb.getProductType() : manufacturerDetails.getProductType());
            manufacturerDetailsDb.setPremisesType((manufacturerDetails.getPremisesType() == null) ? manufacturerDetailsDb.getPremisesType() : manufacturerDetails.getPremisesType());
            manufacturerDetailsDb.setAddress((manufacturerDetails.getAddress() != null) ? updateAddress(manufacturerDetails.getAddress()) : manufacturerDetailsDb.getAddress());
            manufacturerDetailsDb.setModifiedOn(LocalDateTime.now());
            manufacturerDetailsDb = manufacturerDetailsRepository.save(manufacturerDetailsDb);
            return manufacturerDetailsDb;
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        return  new ManufacturerDetails();
    }

    VendorDetails updateVendor(VendorDetails vendorDetails) {
        VendorDetails vendorDetailsDb;
        try {
            vendorDetailsDb = vendorDetailsRepository.findById(vendorDetails.getVendorID()).get();
            vendorDetailsDb.setVendorName(StringUtils.isBlank(vendorDetails.getVendorName()) ? vendorDetailsDb.getVendorName() : vendorDetails.getVendorName());
            vendorDetailsDb.setPhone(StringUtils.isBlank(vendorDetails.getPhone()) ? vendorDetailsDb.getPhone() : vendorDetails.getPhone());
            vendorDetailsDb.setEmail((vendorDetails.getEmail() == null) ? vendorDetailsDb.getEmail() : vendorDetails.getEmail());
            vendorDetailsDb.setPremisesType((vendorDetails.getPremisesType() == null) ? vendorDetailsDb.getPremisesType() : vendorDetails.getPremisesType());
            vendorDetailsDb.setProductType((vendorDetails.getProductType() == null) ? vendorDetailsDb.getProductType() : vendorDetails.getProductType());
            vendorDetailsDb.setAddress((vendorDetails.getAddress() != null) ? updateAddress(vendorDetails.getAddress()) : vendorDetailsDb.getAddress());
            vendorDetailsDb.setModifiedOn(LocalDateTime.now());
            vendorDetailsDb = vendorDetailsRepository.save(vendorDetailsDb);
            return vendorDetailsDb;
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        return new VendorDetails();
    }

}
