package com.jkt.fnolprocess.service.module;

import com.jkt.fnolprocess.domains.ClaimsList;
import com.jkt.fnolprocess.domains.IncidentDetailsPojo;
import com.jkt.fnolprocess.entities.ClaimDetails;
import com.jkt.fnolprocess.util.UtilityDTO;

import java.util.List;

public interface ClaimDetailService {
    void save(ClaimsList claimsList, UtilityDTO utilityDTO);

    void map(List<ClaimsList> claimsLists, UtilityDTO utilityDTO);

    void update(ClaimsList claimsList, UtilityDTO utilityDTO);

    ClaimDetails findByClaimId(String claimId);
}
