package com.jkt.fnolprocess.service.module.impl;

import com.jkt.fnolprocess.domains.ClaimsList;
import com.jkt.fnolprocess.domains.IncidentDetailsPojo;
import com.jkt.fnolprocess.entities.GLInjuredDetails;
import com.jkt.fnolprocess.repositories.GLInjuredDetailsRepository;
import com.jkt.fnolprocess.service.module.GlInjuredDetailsService;
import com.jkt.fnolprocess.service.module.GlLiabilityDetailsService;
import com.jkt.fnolprocess.util.ModuleStatus;
import com.jkt.fnolprocess.util.UtilityDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Slf4j
public class GlInjuredDetailsServiceImpl implements GlInjuredDetailsService {

    @Autowired
    GLInjuredDetailsRepository repository;

    @Autowired
    ServiceHelper helper;

    @Autowired
    GlLiabilityDetailsService glLiabilityDetailsService;

    public void save(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasGlInjuredDetails() && claimsList.getGlInjuredDetails() != null) {
                saveToDB(claimsList, utilityDTO);
                utilityDTO.add(ModuleStatus.success("GL Injured details saved"));
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("GL Injured details save failed :: " + e.getMessage()));
        }
        glLiabilityDetailsService.save(claimsList, utilityDTO);
    }

    private void saveToDB(ClaimsList claimsList, UtilityDTO utilityDTO) {

        GLInjuredDetails glInjuredDetails = claimsList.getGlInjuredDetails();
        glInjuredDetails.setClaimID(utilityDTO.getClaimId());
        glInjuredDetails.setStatus("Active");
        glInjuredDetails.setModifiedBy(utilityDTO.getEmail());
        glInjuredDetails.setModifiedOn(LocalDateTime.now());
        repository.save(glInjuredDetails);
    }

    @Override
    public void map(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasGlInjuredDetails()) {
                Optional<GLInjuredDetails> glInjuredDetails = repository.findByClaimID(utilityDTO.getClaimId());
                glInjuredDetails.ifPresent(claimsList::setGlInjuredDetails);
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        glLiabilityDetailsService.map(claimsList, utilityDTO);
    }

    @Override
    public void update(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasGlInjuredDetails() && claimsList.getGlInjuredDetails() != null) {
                Optional<GLInjuredDetails> glInjuredDetailsDbOptional = repository.findByClaimID(utilityDTO.getClaimId());
                if (glInjuredDetailsDbOptional.isPresent()) {
                    GLInjuredDetails glInjuredDetailsNew = claimsList.getGlInjuredDetails();
                    GLInjuredDetails glInjuredDetailsDb = glInjuredDetailsDbOptional.get();
                    glInjuredDetailsDb.setFirstName(StringUtils.isBlank(glInjuredDetailsNew.getFirstName()) ? glInjuredDetailsDb.getFirstName() : glInjuredDetailsNew.getFirstName());
                    glInjuredDetailsDb.setLastName(StringUtils.isBlank(glInjuredDetailsNew.getLastName()) ? glInjuredDetailsDb.getLastName() : glInjuredDetailsNew.getLastName());
                    glInjuredDetailsDb.setDob((glInjuredDetailsNew.getDob() == null) ? glInjuredDetailsDb.getDob() : glInjuredDetailsNew.getDob());
                    glInjuredDetailsDb.setAge((glInjuredDetailsNew.getAge() == null) ? glInjuredDetailsDb.getAge() : glInjuredDetailsNew.getAge());
                    glInjuredDetailsDb.setSex(StringUtils.isBlank(glInjuredDetailsNew.getSex()) ? glInjuredDetailsDb.getSex() : glInjuredDetailsNew.getSex());

                    glInjuredDetailsDb.setInjuryCode((glInjuredDetailsNew.getInjuryCode()==null) ? glInjuredDetailsDb.getInjuryCode() : glInjuredDetailsNew.getInjuryCode());
                    glInjuredDetailsDb.setInjuryDescription(StringUtils.isBlank(glInjuredDetailsNew.getInjuryDescription()) ? glInjuredDetailsDb.getInjuryDescription() : glInjuredDetailsNew.getInjuryDescription());
                    glInjuredDetailsDb.setPrimaryPhone(StringUtils.isBlank(glInjuredDetailsNew.getPrimaryPhone()) ? glInjuredDetailsDb.getPrimaryPhone() : glInjuredDetailsNew.getPrimaryPhone());
                    glInjuredDetailsDb.setPrimaryPhoneType(StringUtils.isBlank(glInjuredDetailsNew.getPrimaryPhoneType()) ? glInjuredDetailsDb.getPrimaryPhoneType() : glInjuredDetailsNew.getPrimaryPhoneType());
                    glInjuredDetailsDb.setSecondaryPhone(StringUtils.isBlank(glInjuredDetailsNew.getSecondaryPhone()) ? glInjuredDetailsDb.getSecondaryPhone() : glInjuredDetailsNew.getSecondaryPhone());
                    glInjuredDetailsDb.setSecondaryPhoneType(StringUtils.isBlank(glInjuredDetailsNew.getSecondaryPhoneType()) ? glInjuredDetailsDb.getSecondaryPhoneType() : glInjuredDetailsNew.getSecondaryPhoneType());
                    glInjuredDetailsDb.setPrimaryEmail(StringUtils.isBlank(glInjuredDetailsNew.getPrimaryEmail()) ? glInjuredDetailsDb.getPrimaryEmail() : glInjuredDetailsNew.getPrimaryEmail());
                    glInjuredDetailsDb.setSecondaryEmail(StringUtils.isBlank(glInjuredDetailsNew.getSecondaryEmail()) ? glInjuredDetailsDb.getSecondaryEmail() : glInjuredDetailsNew.getSecondaryEmail());

                    glInjuredDetailsDb.setAddress((glInjuredDetailsNew.getAddress() != null) ? helper.updateAddress(glInjuredDetailsNew.getAddress()) : glInjuredDetailsDb.getAddress());
                    glInjuredDetailsDb.setModifiedOn(LocalDateTime.now());
                    glInjuredDetailsDb.setModifiedBy(utilityDTO.getEmail());
                    repository.save(glInjuredDetailsDb);
                } else {
                    saveToDB(claimsList, utilityDTO);
                }
            }
            utilityDTO.add(ModuleStatus.success("GL Injured details updated"));
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("GL Injured details update failed :: " + e.getMessage()));
        }
        glLiabilityDetailsService.update(claimsList, utilityDTO);
    }
}
