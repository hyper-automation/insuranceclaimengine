package com.jkt.fnolprocess.service.module.impl;

import com.jkt.fnolprocess.domains.ClaimsList;
import com.jkt.fnolprocess.domains.IncidentDetailsPojo;
import com.jkt.fnolprocess.entities.IncidentDetails;
import com.jkt.fnolprocess.repositories.IncidentDetailsRepository;
import com.jkt.fnolprocess.service.module.ClaimDetailService;
import com.jkt.fnolprocess.service.module.IncidentDetailsService;
import com.jkt.fnolprocess.util.ModuleStatus;
import com.jkt.fnolprocess.util.UtilityDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class IncidentDetailsServiceImpl implements IncidentDetailsService {

    @Autowired
    IncidentDetailsRepository repository;

    @Autowired
    ClaimDetailService claimDetailService;

    @Override
    public void save(IncidentDetailsPojo incidentDetailsPojo, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasIncidentDetails()) {
                IncidentDetails incidentDetails = new IncidentDetails();
                incidentDetails.setStatus("Active");
                incidentDetails.setModifiedOn(LocalDateTime.now());
                incidentDetails.setIncidentID(utilityDTO.getIncidentId());
                incidentDetails.setSubClaimType((incidentDetailsPojo.getSubClaimType()==null)?1:incidentDetailsPojo.getSubClaimType());
                incidentDetails.setModifiedBy(utilityDTO.getEmail());
                repository.save(incidentDetails);
                utilityDTO.add(ModuleStatus.success("Incident details saved"));
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("Incident details save Failed" + e.getMessage()));
            utilityDTO.exitFlow();
        }
        for (ClaimsList claimsList:incidentDetailsPojo.getClaimsLists()) {
            claimDetailService.save(claimsList, utilityDTO);
        }
     //   claimDetailService.save(, utilityDTO);
    }

    @Override
    public void update(IncidentDetailsPojo incidentDetailsPojo, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasIncidentDetails() ) {
                Optional<IncidentDetails> incidentDetails=repository.findByIncidentID(utilityDTO.getIncidentId());
                if(incidentDetails.isPresent()){
                   IncidentDetails incidentDetailsDb=incidentDetails.get();
                   incidentDetailsDb.setSubClaimType((incidentDetailsPojo.getSubClaimType()==null)?incidentDetailsDb.getSubClaimType():incidentDetailsDb.getSubClaimType());
                   incidentDetailsDb.setModifiedBy(StringUtils.isBlank(utilityDTO.getEmail())?incidentDetailsDb.getModifiedBy(): utilityDTO.getEmail());
                   incidentDetailsDb.setModifiedOn(LocalDateTime.now());
                   repository.save(incidentDetailsDb);
                }
            }
            utilityDTO.add(ModuleStatus.success("Incident details updated"));
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("Incident details update Failed" + e.getMessage()));
        }
        for (ClaimsList claimsList:incidentDetailsPojo.getClaimsLists()) {
            claimDetailService.update(claimsList, utilityDTO);

        }
    }

    @Override
    public void map(IncidentDetailsPojo incidentDetailsPojo, UtilityDTO utilityDTO) {
        try {

            if (utilityDTO.isHasIncidentDetails()) {
                Optional<IncidentDetails> incidentDetails = repository.findByIncidentID(utilityDTO.getIncidentId());
                incidentDetails.ifPresent(details -> incidentDetailsPojo.setSubClaimType((details.getSubClaimType()!=null)?details.getSubClaimType():1));
                incidentDetailsPojo.setIncidentId(utilityDTO.getIncidentId());
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        List<ClaimsList> claimsLists = new ArrayList<>();
        claimDetailService.map(claimsLists, utilityDTO);
        incidentDetailsPojo.setClaimsLists(claimsLists);
    }

    @Override
    public List<IncidentDetails> getAllIncidentDetails() {
        try {
            return repository.findAll();
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        return new ArrayList<>();

    }

}
