package com.jkt.fnolprocess.service.module.impl;

import com.jkt.fnolprocess.domains.ClaimsList;
import com.jkt.fnolprocess.domains.IncidentDetailsPojo;
import com.jkt.fnolprocess.entities.*;
import com.jkt.fnolprocess.repositories.EmployeeRepository;
import com.jkt.fnolprocess.repositories.GLClaimantDetailsRepository;
import com.jkt.fnolprocess.repositories.GLInsuredDetailsRepository;
import com.jkt.fnolprocess.repositories.InsuredDetailsRepository;
import com.jkt.fnolprocess.service.module.GlClaimantDetailsService;
import com.jkt.fnolprocess.service.module.GlInjuredDetailsService;
import com.jkt.fnolprocess.util.ModuleStatus;
import com.jkt.fnolprocess.util.UtilityDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Slf4j
public class GlClaimantDetailsServiceImpl implements GlClaimantDetailsService {

    @Autowired
    GLClaimantDetailsRepository repository;

    @Autowired
    GLInsuredDetailsRepository glInsuredDetailsRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    GlInjuredDetailsService glInjuredDetailsService;

    @Override
    public void save(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasGlClaimantDetails() && claimsList.getGlClaimantDetails() != null) {
                GLClaimantDetails glClaimantDetails = claimsList.getGlClaimantDetails();
                saveToDB(utilityDTO, glClaimantDetails);
                utilityDTO.add(ModuleStatus.success("GL Claimant details saved"));
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("GL Claimant details save failed :: " + e.getMessage()));
        }
        glInjuredDetailsService.save(claimsList, utilityDTO);
    }

    private void saveToDB(UtilityDTO utilityDTO, GLClaimantDetails glClaimantDetails) {
        GLInsuredDetails glInsuredDetails = glInsuredDetailsRepository.findByPrimaryEmail(glClaimantDetails.getPrimaryEmail()).get();
        //EmployeeDetails employeeDetails = employeeRepository.findByInsuredID(glInsuredDetails.getInsuredID()).get();
        glClaimantDetails.setClaimID(utilityDTO.getClaimId());
        glClaimantDetails.setInsuredName(glInsuredDetails.getInsuredName());
        glClaimantDetails.setDateOfIncorporation(glInsuredDetails.getDateOfIncorporation());
        glClaimantDetails.setEmployementStatus("Active");
        glClaimantDetails.setFeinNumber(glInsuredDetails.getFeinNumber());
        glClaimantDetails.setPrimaryPhone(glInsuredDetails.getPrimaryPhone());
        glClaimantDetails.setSecondaryPhone(glInsuredDetails.getSecondaryPhone());
        glClaimantDetails.setPrimaryEmail(glInsuredDetails.getPrimaryEmail());
        glClaimantDetails.setSecondaryEmail(glInsuredDetails.getSecondaryEmail());
        glClaimantDetails.setStatus("Active");
        glClaimantDetails.setModifiedBy(glInsuredDetails.getPrimaryEmail());
        glClaimantDetails.setModifiedOn(LocalDateTime.now());
        repository.save(glClaimantDetails);
    }

    @Override
    public void map(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasGlClaimantDetails()) {
                Optional<GLClaimantDetails> glClaimantDetails = repository.findTopByClaimIDOrderByModifiedOnDesc(utilityDTO.getClaimId());
                glClaimantDetails.ifPresent(claimsList::setGlClaimantDetails);
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        glInjuredDetailsService.map(claimsList, utilityDTO);
    }

    @Override
    public void update(ClaimsList claimsList, UtilityDTO utilityDTO) {
        GLClaimantDetails glClaimantDetails=claimsList.getGlClaimantDetails();
        if(glClaimantDetails!=null){
            saveToDB(utilityDTO,glClaimantDetails);
        }
        // GL claiment details update is not required to update so directly passed to next flow #GL Injured Details
        glInjuredDetailsService.update(claimsList, utilityDTO);
    }
}
