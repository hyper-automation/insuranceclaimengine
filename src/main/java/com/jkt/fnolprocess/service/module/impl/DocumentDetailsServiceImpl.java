package com.jkt.fnolprocess.service.module.impl;

import com.jkt.fnolprocess.domains.ClaimsList;
import com.jkt.fnolprocess.domains.IncidentDetailsPojo;
import com.jkt.fnolprocess.entities.DocumentDetails;
import com.jkt.fnolprocess.entities.SourceType;
import com.jkt.fnolprocess.repositories.DocumentRepository;
import com.jkt.fnolprocess.service.module.DocumentDetailsService;
import com.jkt.fnolprocess.service.module.WcOccuranceDetailsService;
import com.jkt.fnolprocess.util.ModuleStatus;
import com.jkt.fnolprocess.util.UtilityDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DocumentDetailsServiceImpl implements DocumentDetailsService {

    @Autowired
    DocumentRepository repository;

    @Autowired
    WcOccuranceDetailsService wcOccuranceDetailsService;

    @Override
    public void save(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasDocumentDetails()  && claimsList.getDocumentDetails()!=null && !claimsList.getDocumentDetails().isEmpty()) {

                List<DocumentDetails> documentDetailsList = claimsList.getDocumentDetails()
                        .stream()
                        .map(documentDetails -> {
                            documentDetails.setClaimID(utilityDTO.getClaimId());
                            documentDetails.setStatus("Active");
                            documentDetails.setModifiedBy(utilityDTO.getEmail());
                            documentDetails.setModifiedOn(LocalDateTime.now());
                            return documentDetails;
                        })
                        .collect(Collectors.toList());
                repository.saveAll(documentDetailsList);
               utilityDTO.add(ModuleStatus.success("Document details saved"));
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("Document details save failed :: " + e.getMessage()));
        }
        wcOccuranceDetailsService.save(claimsList, utilityDTO);
    }

    @Override
    public void map(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasDocumentDetails()) {
                List<DocumentDetails> documentDetailsList = repository.findByClaimID(utilityDTO.getClaimId());
                claimsList.setDocumentDetails(documentDetailsList);
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        wcOccuranceDetailsService.map(claimsList, utilityDTO);
    }

    @Override
    public void update(ClaimsList claimsList, UtilityDTO utilityDTO) {
        try {
            if (utilityDTO.isHasDocumentDetails() && claimsList.getDocumentDetails() != null && !claimsList.getDocumentDetails().isEmpty()) {
                List<DocumentDetails> documents = claimsList.getDocumentDetails();
                for (DocumentDetails document : documents) {
                    if (document.getDocumentID() != null) {
                        DocumentDetails documentDetailsDb = repository.findById(document.getDocumentID()).get();
                        documentDetailsDb.setDocumentURL(StringUtils.isBlank(document.getDocumentURL()) ? documentDetailsDb.getDocumentURL() : document.getDocumentURL());
                        documentDetailsDb.setDocumentFormat(StringUtils.isBlank(document.getDocumentFormat()) ? documentDetailsDb.getDocumentFormat() : document.getDocumentFormat());
                        documentDetailsDb.setDocumentTitle(StringUtils.isBlank(document.getDocumentTitle()) ? documentDetailsDb.getDocumentTitle() : document.getDocumentTitle());
                        documentDetailsDb.setDocumentType((document.getDocumentType() == null) ? documentDetailsDb.getDocumentType() : document.getDocumentType());
                        documentDetailsDb.setModifiedOn(LocalDateTime.now());
                        document.setModifiedBy(utilityDTO.getEmail());
                        repository.save(documentDetailsDb);
                    } else {
                        document.setClaimID(utilityDTO.getClaimId());
                        document.setStatus("Active");
                        document.setModifiedBy(utilityDTO.getEmail());
                        document.setModifiedOn(LocalDateTime.now());
                        repository.save(document);
                    }
                }
            }
            utilityDTO.add(ModuleStatus.success("Document details saved"));
        }catch (Exception e){
            log.error(e.getMessage(),e);
            utilityDTO.add(ModuleStatus.failed("Document details update failed :: " + e.getMessage()));
        }
        wcOccuranceDetailsService.update(claimsList, utilityDTO);
    }

    @Override
    public void get(DocumentDetails documentDetails,Integer id) {
        try {
            DocumentDetails documentDetailsNew = repository.findById(id).get();
            documentDetails.setDocumentFormat(documentDetailsNew.getDocumentFormat());
            documentDetails.setDocumentURL(documentDetailsNew.getDocumentURL());
            documentDetails.setDocumentType(documentDetailsNew.getDocumentType());
            documentDetails.setDocumentTitle(documentDetailsNew.getDocumentTitle());
        }
        catch (Exception e){
            log.error(e.getMessage(),e);
        }
    }

    @Override
    public void updateClaimId(String id, String claimId) {
        try {
            DocumentDetails documentDetails = repository.findById(Integer.parseInt(id)).get();
            documentDetails.setClaimID(claimId);
            repository.save(documentDetails);
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
    }
}
