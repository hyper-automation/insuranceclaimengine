package com.jkt.fnolprocess.service.module;

import com.jkt.fnolprocess.domains.ClaimsList;
import com.jkt.fnolprocess.domains.IncidentDetailsPojo;
import com.jkt.fnolprocess.util.UtilityDTO;

public interface GlLiabilityDetailsService {
    void save(ClaimsList claimsList, UtilityDTO utilityDTO);

    void map(ClaimsList claimsList, UtilityDTO utilityDTO);

    void update(ClaimsList claimsList, UtilityDTO utilityDTO);
}
