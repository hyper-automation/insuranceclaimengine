package com.jkt.fnolprocess.service;

import com.jkt.fnolprocess.domains.*;
import com.jkt.fnolprocess.util.ClaimSourceTypeEnum;
import com.jkt.fnolprocess.util.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClaimService {

    @Autowired
    ClaimServiceHelper claimServiceHelper;

    public ResponseDTO save(Claims claim) {

        IncidentDetailsPojo incidentDetailsPojo = claim.getIncidentDetails();
        switch (incidentDetailsPojo.getClaimsLists().get(0).getClaimDetails().getClaimSourceType()) {
            case 1:
                return claimServiceHelper.saveClaim(incidentDetailsPojo, ClaimSourceTypeEnum.WEB);
            case 2:
                return claimServiceHelper.saveClaim(incidentDetailsPojo, ClaimSourceTypeEnum.EMAIL);
            case 3:
                return claimServiceHelper.saveClaim(incidentDetailsPojo, ClaimSourceTypeEnum.CHATBOT);
            default:
                ResponseDTO responseDTO = new ResponseDTO();
                responseDTO.setMessages(null);
                return responseDTO;
        }
    }

    public Claims getByIncidentId(String incidentID) {
        return claimServiceHelper.getClaim(incidentID);
    }

    public ResponseDTO update(Claims claim) {
        return claimServiceHelper.updateClaim(claim.getIncidentDetails());
    }

    public List<Claims> getByEmployee(Integer employeeId) {
        return claimServiceHelper.getByEmployee(employeeId);
    }

    public List<Claims> getByEmployer(Integer employerId) {
        return claimServiceHelper.getByEmployer(employerId);
    }

    public List<Claims> getAll() {
        return claimServiceHelper.getAllClaims();
    }

    public List<Claims> getByInsured(Integer insuredID) {
        return claimServiceHelper.getByInsured(insuredID);
    }
}

