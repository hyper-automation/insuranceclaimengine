package com.jkt.fnolprocess.domains;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class Claims {
    IncidentDetailsPojo incidentDetails;
    public Claims setIncidentDetails(IncidentDetailsPojo incidentDetails) {
        this.incidentDetails = incidentDetails;
        return this;
    }
}
