package com.jkt.fnolprocess.domains;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Data
public class ClaimDetailsPojo {
    private Integer claimID;
    private int claimType;
    private String policyNumber;
    private int claimStatus;
    private int claimSourceType;
    private String claimCreatedBy;
    private Date claimCreatedOn;
    private int version;
    private String status;
    private String modifiedBy;
    private Date modifiedOn;
}
