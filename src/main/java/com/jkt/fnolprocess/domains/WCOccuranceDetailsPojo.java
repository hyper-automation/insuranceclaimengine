package com.jkt.fnolprocess.domains;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Data
public class WCOccuranceDetailsPojo {

    private int occuranceID;

    private Integer claimID;

    private Address address;

    private Date lastWorkDate;

    private Date employerNotifiedDate;

    private String didOccuredOnEmpPremises;

    private String occurredDepartment;

    private String equipmentList;

    private String specificActivity;

    private String safetyEqupProvided;

    private String safetyEqupUsed;

    private String occurrenceDetails;

    private String workProcess;

    private String eventSequence;

    private Date ocurrenceTime;

    private String status;

    private String modifiedBy;

    private Date modifiedOn;
}
