package com.jkt.fnolprocess.domains;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Data
public class PropertyDetailsPojo {

    private int propertyID;

    private int claimID;

    private String ownerName;

    private Address address;

    private String phone;

    private String email;

    private String describeProperty;

    private int estimateAmount;

    private String propertyLocation;

    private String status;

    private String modifiedBy;

    private Date modifiedOn;
}
