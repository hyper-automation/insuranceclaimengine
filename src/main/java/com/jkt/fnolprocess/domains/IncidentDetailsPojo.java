package com.jkt.fnolprocess.domains;

import com.jkt.fnolprocess.entities.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Data
public class IncidentDetailsPojo {
    String incidentId;
    Integer subClaimType;
    List<ClaimsList> claimsLists;
    /*
    ClaimDetails claimDetails;
    List<DocumentDetails> documentDetails;
    GLOccuranceDetails glOccuranceDetails;
    List<InjuryDetailsPojo> injuryDetails;
    GLLiabilityDetails glLiabilityDetails;
    GLInjuredDetails glInjuredDetails;
    GLClaimantDetails glClaimantDetails;
    PropertyDetails propertyDetails;
    List<WitnessDetails> witnessDetails;
    WCOccuranceDetails wcOccuranceDetails;
    WCClaimantDetails wcClaimantDetails;*/
}
