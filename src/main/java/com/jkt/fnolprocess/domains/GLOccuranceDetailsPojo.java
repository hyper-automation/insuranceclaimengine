package com.jkt.fnolprocess.domains;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Data
public class GLOccuranceDetailsPojo {

    private int occuranceID;

    private int claimID;

    private Address address;

    private Date employerNotifiedDate;

    private String occurredDepartment;

    private String equipmentList;

    private String specificActivity;

    private boolean policeContacted;

    private String reportNumber;

    private String occurrenceDetails;

    private String eventSequence;

    private Date ocurrenceTime;

    private String status;

    private String modifiedBy;

    private Date modifiedOn;


}
