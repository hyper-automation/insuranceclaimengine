package com.jkt.fnolprocess.domains;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class Address {

    private int addressID;

    private String addressName;

    private String addressCity;

    private int addressState;

    private int addressCountry;

    private String addressZip;
}


