package com.jkt.fnolprocess.domains;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Data
public class GlLiabilityDetailsPojo {

    private int liabilityID;

    private int claimID;

    private String premisesInsured;

    private String ownerName;

    private Address address;

    private String productInsured;

    private String whereBeSeen;

    private String typeOfPremises;

    private String phone;

    private String email;

    private VendorDetailsPojo vendor;

    private ManufacturerDetailsPojo manufactuter;

    private String status;

    private String modifiedBy;

    private Date modifiedOn;
}
