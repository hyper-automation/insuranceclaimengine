package com.jkt.fnolprocess.domains;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Data
public class ManufacturerDetailsPojo {

    private int manufacturerID;

    private String manufacturerName;

    private Address address;

    private int productType;

    private int premisesType;

    private String phone;

    private String email;

    private String status;

    private String modifiedBy;

    private Date modifiedOn;
}
