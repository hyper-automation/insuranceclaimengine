package com.jkt.fnolprocess.domains;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class EmailResponse {
    private String email_from;
    private String email_body;
    private String document_url;
    private boolean email_extraction;
    private boolean document_extraction;

    public EmailResponse(String email_From,String email_body, String document_url, boolean email_extraction, boolean document_extraction) {
        this.email_from=email_From;
        this.email_body = email_body;
        this.document_url = document_url;
        this.email_extraction = email_extraction;
        this.document_extraction = document_extraction;
    }
}
