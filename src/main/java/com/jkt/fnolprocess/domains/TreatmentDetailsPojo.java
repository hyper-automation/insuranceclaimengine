package com.jkt.fnolprocess.domains;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Data
@Setter
@Getter
public class TreatmentDetailsPojo {


    private int treatmentID;

    private int injuryID;

    private String physicianName;

    private Address address;

    private String hospName;

    private String initialTreatment;

    private Date administratorNotifiedDate;

    private String status;

    private String modifiedBy;

    private Date modifiedOn;
}
