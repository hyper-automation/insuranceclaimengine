package com.jkt.fnolprocess.domains;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@Data
public class GLInjuredDetailsPojo {


    private int injuredID;

    private int claimID;

    private String firstName;

    private String lastName;

    private Date dob;

    private int age;

    private String sex;

    private String maritalstatus;

    private String phone;

    private String email;

    private Address address;

    private String status;

    private String modifiedBy;

    private Date modifiedOn;
}
