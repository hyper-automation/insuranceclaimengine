package com.jkt.fnolprocess.domains;

import com.jkt.fnolprocess.entities.DocumentDetails;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

@Getter
@Setter
@Data
public class DocumentDetailsPojo {

    private int documentID;

    private int claimID;

    private String documentTitle;

    private String documentURL;

    private int documentType;

    private String documentFormat;

    private String status;

    private String modifiedBy;

    private Date modifiedOn;

    public static DocumentDetails getEntity() {
        return new DocumentDetails();
    }
}
