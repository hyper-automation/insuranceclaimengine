package com.jkt.fnolprocess.domains;

import com.jkt.fnolprocess.entities.TreatmentDetails;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Data
public class InjuryDetailsPojo {

    private Integer injuryID;

    private String claimID;

    private Date injuryDate;

    private Integer injuryCause;

    private Integer injuryType;

    private Integer bodyCode;

    private String fatalStatus;

    private Date deathDate;

    private String beginWork;

    private Date returnDate;

    private Date disabilityBeganDate;

    private String status;

    private String modifiedBy;

    private LocalDateTime modifiedOn;

    private List<TreatmentDetails> treatmentDetails;
}
