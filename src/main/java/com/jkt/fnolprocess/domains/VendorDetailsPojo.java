package com.jkt.fnolprocess.domains;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Data
@Getter
@Setter
public class VendorDetailsPojo {

    private int vendorID;

    private String vendorName;

    private Address address;

    private String productType;

    private String premisesType;

    private String phone;

    private String email;

    private String status;

    private String modifiedBy;

    private Date modifiedOn;
}
