package com.jkt.fnolprocess.domains;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Data
public class WCClaimantDetailsPojo {

    private int claimantID;

    private int ClaimID;

    private String firstName;

    private String lastName;

    private Date dob;

    private int age;

    private String sex;

    private String maritalstatus;

    private String occupation;

    private String employementStatus;

    private String feinNumber;

    private String primaryPhone;

    private String secondaryPhone;

    private String primaryEmail;

    private String secondaryEmail;

    private int numOfDependents;

    private String employeeCode;

    private int SSN_Number;

    private Date dateHired;

    private String stateHired;

    private String ncciCodeID;

    private String wageRate;

    private String wageFrequencyID;

    private int avgWeeklyWages;

    private int noOfDaysPerWeek;

    private String fullPayDayOfInjury;

    private String salaryStatus;

    private String whereTaken;

    private String status;

    private String modifiedBy;

    private Date modifiedOn;
}
