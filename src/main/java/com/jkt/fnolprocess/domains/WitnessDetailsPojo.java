package com.jkt.fnolprocess.domains;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Data
@Getter
public class WitnessDetailsPojo {
    private int witnessID;

    private int claimID;

    private String witnessName;

    private Address address;

    private String phone;

    private String email;

    private String status;

    private String modifiedBy;

    private Date modifiedOn;
}
