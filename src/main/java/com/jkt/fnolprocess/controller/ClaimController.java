package com.jkt.fnolprocess.controller;

import com.jkt.fnolprocess.domains.Claims;
import com.jkt.fnolprocess.repositories.StateRepository;
import com.jkt.fnolprocess.service.ClaimService;
import com.jkt.fnolprocess.util.ResponseDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name="FNOL",description = "InsuranceClaimEngine- Transactional Api")
public class ClaimController {
    @Autowired
    ClaimService claimService;


    @Autowired
    StateRepository repo;

    @CrossOrigin(origins = "*")
    @PostMapping("/saveclaim")
    @Operation(summary = "Save Claim api will create claim as per the Claim Type provided in the RequestBody.")
    public ResponseDTO saveClaim(@io.swagger.v3.oas.annotations.parameters.RequestBody( description= "Claim information from the Email Body or Claim Form") @RequestBody Claims claim) {
        return claimService.save(claim);
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Get All the Claims")
    public List<Claims> getAllClaim() {
        return claimService.getAll();
    }

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get Claim information Incident")
    @GetMapping(value = "/get/{incidentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Claims getClaim(@Parameter(description = "Incident id") @PathVariable String incidentId) {
        return claimService.getByIncidentId(incidentId);
    }

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the claims of employee")
    @GetMapping(value = "/get/byEmployee/{employeeId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Claims> getClaimbyEmployee(@Parameter(description = "Employee Id") @PathVariable Integer employeeId) {
        return claimService.getByEmployee(employeeId);
    }

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the claims of employer")
    @GetMapping(value = "/get/byEmployer/{employerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Claims> getClaimbyEmployer(@Parameter(description = "Employer Id") @PathVariable Integer employerId) {
        return claimService.getByEmployer(employerId);
    }

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the claims assigned to Insurer")
    @GetMapping(value = "/get/byInsured/{insuredId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Claims> getClaimbyInsured(@Parameter(description = "Insurer Id")@PathVariable Integer insuredId) {
        return claimService.getByInsured(insuredId);
    }

    @CrossOrigin(origins = "*")
    @Operation(summary = "Update the claim information once the claim approved")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseDTO updateClaim(@io.swagger.v3.oas.annotations.parameters.RequestBody( description= "Claim information once claim approved")@RequestBody Claims claim) {
        return claimService.update(claim);
    }


}

