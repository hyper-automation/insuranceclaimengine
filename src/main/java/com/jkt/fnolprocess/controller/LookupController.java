package com.jkt.fnolprocess.controller;

import com.jkt.fnolprocess.entities.*;
import com.jkt.fnolprocess.service.LookupService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name="FNOL",description = "InsuranceClaimEngine- Master Data Api")
public class LookupController {
    @Autowired
    LookupService service;

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the claimType (GL/WC)")
    @GetMapping(value = "/claimType", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ClaimType> getClaimType() {
        return service.getClaimType();
    }
    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the claim status")
    @GetMapping(value = "/claimStatus", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ClaimStatusDetails> getClaimStatus() {
        return service.getClaimStatus();
    }

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the source types like (web/email/chat")
    @GetMapping(value = "/sourceType", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SourceType> getSourceType() {
        return service.getSourceType();
    }
    @CrossOrigin(origins = "*")
    @Operation(summary = "Get the all the document types medical form or claim form")
    @GetMapping(value = "/documentType", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DocumentType> getDocumentType() {
        return service.getDocumentType();
    }
    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the marital status ")
    @GetMapping(value = "/maritalStatus", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MaritalStatus> getMaritalStatus() {
        return service.getMaritalStatus();
    }
    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the occupations")
    @GetMapping(value = "/occupations", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OccupationDetails> getOccupations() {
        return service.getOccupations();
    }
    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the wages Frequency")
    @GetMapping(value = "/wagesFrequency", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<WagesFrequencyDetails> getWagesFrequency() {
        return service.getWagesFrequency();
    }
    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the Injury Codes based on the category")
    @GetMapping(value = "/injuryCodes/{category}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<InjuryCodeDetails> getInjuryCodes(@Parameter(description = "Category Id") @PathVariable Integer category) {
        return service.getInjuryCodes(category);
    }
    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the Injury Codes")
    @GetMapping(value = "/injuryCodes", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<InjuryCodeDetails> getAllInjuryCodes() {
        return service.getAllInjuryCodes();
    }

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the Body Codes based on the category")
    @GetMapping(value = "/bodyCodes/{category}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BodyCodeDetails> getBodyCodes(@Parameter(description = "Category Id") @PathVariable Integer category) {
        return service.getBodyCodes(category);
    }

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the Body Codes")
    @GetMapping(value = "/bodyCodes", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BodyCodeDetails> getAllBodyCodes() {
        return service.getAllBodyCodes();
    }

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the naicCodes based on the category")
    @GetMapping(value = "/naicCodes/{category}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NaicCodeDetails> getNaicCodes(@Parameter(description = "Category Id") @PathVariable Integer category) {
        return service.getNaicCodes(category);
    }
    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the ncciCodes")
    @GetMapping(value = "/ncciCodes", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NcciCodeDetails> getNcciCodes() {
        return service.getNcciCodes();
    }
    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the Injury causes")
    @GetMapping(value = "/injuryCauses", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<InjuryCauseDetails> getInjuryCauses() {
        return service.getInjuryCauses();
    }

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the Industry codes")
    @GetMapping(value = "/industryCodes", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<IndustryCodeDetails> getIndustryCodes() {
        return service.getIndustryCodes();
    }

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the product Type")
    @GetMapping(value = "/productType", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProductTypeDetails> getProductType() {
        return service.getProductType();
    }
    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the premises Type")
    @GetMapping(value = "/premisesType", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PremisesTypeDetails> getPremisesType() {
        return service.getPremisesType();
    }
    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the Countries")
    @GetMapping(value = "/countries", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CountryDetails> getCountries() {
        return service.getCountries();
    }
    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the States in the selected Country")
    @GetMapping(value = "/states/{country}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<StateDetails> getStates(@Parameter(description = "Country Id") @PathVariable Integer country) {
        return service.getStates(country);
    }

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the States")
    @GetMapping(value = "/states", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<StateDetails> getAllStates() {
        return service.getAllStates();
    }

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get all the SubClain Types")
    @GetMapping(value = "/subClaimType", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SubClaimType> getSubClaimType() {
        return service.getSubClaimType();
    }
}
