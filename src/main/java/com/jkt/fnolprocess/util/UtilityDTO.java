package com.jkt.fnolprocess.util;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class UtilityDTO {

    String claimId;
    String incidentId;
    String email;
    String uniqueKey;
    boolean hasIncidentDetails;
    boolean hasClaimDetails;
    boolean hasDocumentDetails;
    boolean hasWitnessDetails;
    boolean hasPropertyDetails;
    boolean hasInjuryDetails;

    boolean hasWcClaimantDetails;
    boolean hasWCOccuranceDetails;

    boolean hasGlClaimantDetails;
    boolean hasGlLiabilityDetails;
    boolean hasGlInjuredDetails;
    boolean hasGLOccuranceDetails;

    List<ModuleStatus> moduleStatuses;
    boolean actionSuccess = true;

    public UtilityDTO() {
        createUniqueKey();
        this.hasIncidentDetails = true;
        this.hasClaimDetails = true;
    }

    public void createWCDefault(ClaimSourceTypeEnum claimSourceTypeEnum) {
        if (!claimSourceTypeEnum.equals(ClaimSourceTypeEnum.DEFAULT)) {
            this.claimId = generatorForClaimId();
            this.incidentId = generatorForIncidentId();

        }
        switch (claimSourceTypeEnum) {
            case WEB:
                this.hasDocumentDetails = true;
                this.hasInjuryDetails = true;
                this.hasPropertyDetails = false;
                this.hasWcClaimantDetails = true;
                this.hasWitnessDetails = true;
                this.hasWCOccuranceDetails = true;
                this.hasGlClaimantDetails = false;
                this.hasGLOccuranceDetails = false;
                this.hasGlInjuredDetails = false;
                this.hasGlLiabilityDetails = false;
                break;
            case EMAIL:
                this.hasDocumentDetails = true;
                this.hasInjuryDetails = true;
                this.hasPropertyDetails = false;
                this.hasWcClaimantDetails = true;
                this.hasWitnessDetails = true;
                this.hasWCOccuranceDetails = true;
                this.hasGlClaimantDetails = false;
                this.hasGLOccuranceDetails = false;
                this.hasGlInjuredDetails = false;
                this.hasGlLiabilityDetails = false;
                break;
            case CHATBOT:
                this.hasDocumentDetails = true;
                this.hasInjuryDetails = true;
                this.hasPropertyDetails = false;
                this.hasWcClaimantDetails = true;
                this.hasWitnessDetails = true;
                this.hasWCOccuranceDetails = true;
                this.hasGlClaimantDetails = false;
                this.hasGLOccuranceDetails = false;
                this.hasGlInjuredDetails = false;
                this.hasGlLiabilityDetails = false;
                break;
            case DEFAULT:
                this.hasDocumentDetails = true;
                this.hasInjuryDetails = true;
                this.hasPropertyDetails = false;
                this.hasWcClaimantDetails = true;
                this.hasWitnessDetails = true;
                this.hasWCOccuranceDetails = true;
                this.hasGlClaimantDetails = false;
                this.hasGLOccuranceDetails = false;
                this.hasGlInjuredDetails = false;
                this.hasGlLiabilityDetails = false;
                break;
        }
    }


    public void createGLDefault(ClaimSourceTypeEnum claimSourceTypeEnum) {
        if (!claimSourceTypeEnum.equals(ClaimSourceTypeEnum.DEFAULT)) {
            this.incidentId = generatorForIncidentId();

        }
        switch (claimSourceTypeEnum) {
            case WEB:
                this.hasDocumentDetails = true;
                this.hasInjuryDetails = false;
                this.hasPropertyDetails = true;
                this.hasWcClaimantDetails = false;
                this.hasWitnessDetails = true;
                this.hasWCOccuranceDetails = false;
                this.hasGlClaimantDetails = true;
                this.hasGLOccuranceDetails = true;
                this.hasGlInjuredDetails = true;
                this.hasGlLiabilityDetails = true;
                break;
            case EMAIL:
                this.hasDocumentDetails = true;
                this.hasInjuryDetails = false;
                this.hasPropertyDetails = true;
                this.hasWcClaimantDetails = false;
                this.hasWitnessDetails = true;
                this.hasWCOccuranceDetails = false;
                this.hasGlClaimantDetails = true;
                this.hasGLOccuranceDetails = true;
                this.hasGlInjuredDetails = true;
                this.hasGlLiabilityDetails = true;
                break;
            case CHATBOT:
                this.hasDocumentDetails = true;
                this.hasInjuryDetails = false;
                this.hasPropertyDetails = true;
                this.hasWcClaimantDetails = false;
                this.hasWitnessDetails = true;
                this.hasWCOccuranceDetails = false;
                this.hasGlClaimantDetails = true;
                this.hasGLOccuranceDetails = true;
                this.hasGlInjuredDetails = true;
                this.hasGlLiabilityDetails = true;
                break;
            case DEFAULT:
                this.hasDocumentDetails = true;
                this.hasInjuryDetails = false;
                this.hasPropertyDetails = true;
                this.hasWcClaimantDetails = false;
                this.hasWitnessDetails = true;
                this.hasWCOccuranceDetails = false;
                this.hasGlClaimantDetails = true;
                this.hasGLOccuranceDetails = true;
                this.hasGlInjuredDetails = true;
                this.hasGlLiabilityDetails = true;
                break;
        }
    }

    public String generatorForClaimId() {
        return "CLID" + createUniqueKey();
    }

    private String generatorForIncidentId() {
        return "I" + this.uniqueKey;
    }

    private String createUniqueKey() {
        this.uniqueKey = Long.toString(System.currentTimeMillis());
        return this.uniqueKey;
    }

    public void add(ModuleStatus status){
        if(this.moduleStatuses == null){
            moduleStatuses = new ArrayList<>();
        }
        moduleStatuses.add(status);
    }

    public void exitFlow() {
        clearFlags();
        this.setActionSuccess(false);
    }

    private void clearFlags() {
        this.hasDocumentDetails = false;
        this.hasInjuryDetails = false;
        this.hasPropertyDetails = false;
        this.hasWcClaimantDetails = false;
        this.hasWitnessDetails = false;
        this.hasWCOccuranceDetails = false;
        this.hasGlClaimantDetails = false;
        this.hasGLOccuranceDetails = false;
        this.hasGlInjuredDetails = false;
        this.hasGlLiabilityDetails = false;
        this.hasIncidentDetails = false;
        this.hasClaimDetails = false;
    }

    public List<String> getAllMessages(){
        return this.getModuleStatuses().stream()
                .filter(status -> !status.isSuccess())
                .map(ModuleStatus::getErrorMessage)
                .collect(Collectors.toList());
    }
}
