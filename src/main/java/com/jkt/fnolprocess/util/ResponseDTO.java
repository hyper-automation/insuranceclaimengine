package com.jkt.fnolprocess.util;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ResponseDTO {
    List<String> messages;
    String incidentId;
    List<String> claimId;
    String claimType;

    public void add(String message){
        if(this.messages == null){
            messages = new ArrayList<>();
        }
        this.messages.add(message);
    }
}
