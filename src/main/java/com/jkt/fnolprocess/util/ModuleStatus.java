
package com.jkt.fnolprocess.util;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ModuleStatus {
    private boolean success;
    private String errorMessage;
    private String successMessage;

    public static ModuleStatus success(String successMessage){
        ModuleStatus status = new ModuleStatus();
        status.setSuccess(true);
        status.setSuccessMessage(successMessage);
        return status;
    }

    public static ModuleStatus failed(String errorMessage){
        ModuleStatus status = new ModuleStatus();
        status.setSuccess(false);
        status.setErrorMessage(errorMessage);
        return status;
    }

}


