package com.jkt.fnolprocess.util;

public enum ClaimSourceTypeEnum {
    WEB,
    EMAIL,
    CHATBOT,
    DEFAULT
}
