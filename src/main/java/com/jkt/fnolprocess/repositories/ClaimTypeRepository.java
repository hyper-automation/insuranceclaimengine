package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.ClaimType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClaimTypeRepository extends JpaRepository<ClaimType, Integer> {
   Optional<ClaimType> findByClaimType(String claimType);
}
