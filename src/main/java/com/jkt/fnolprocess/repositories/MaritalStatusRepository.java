package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.MaritalStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MaritalStatusRepository extends JpaRepository<MaritalStatus, Integer> {

    Optional<MaritalStatus> findByMaritalStatus(String maritalStatus);
}
