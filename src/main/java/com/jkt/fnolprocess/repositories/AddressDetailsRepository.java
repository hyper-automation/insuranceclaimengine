package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.AddressDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressDetailsRepository extends JpaRepository<AddressDetails, Integer> {
}
