package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.DocumentDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentRepository extends JpaRepository<DocumentDetails,Integer> {
    List<DocumentDetails> findByClaimID(String claimID);
}
