package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.WagesFrequencyDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WagesFrequencyRepository extends JpaRepository<WagesFrequencyDetails, Integer> {

    Optional<WagesFrequencyDetails> findByWageDescription(String wageDescription);
}
