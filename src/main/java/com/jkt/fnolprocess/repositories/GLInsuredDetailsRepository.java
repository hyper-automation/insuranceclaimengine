package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.GLInsuredDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GLInsuredDetailsRepository extends JpaRepository<GLInsuredDetails, Integer> {
    Optional<GLInsuredDetails> findByPrimaryEmail(String primaryEmail);
}
