package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.InsuredDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Optional;

@Repository
public interface InsuredDetailsRepository extends JpaRepository<InsuredDetails , Integer> {
    Optional<InsuredDetails> findByPrimaryEmail(String primaryEmail);
}
