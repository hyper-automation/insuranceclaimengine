package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.ProductTypeDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductTypeRepository extends JpaRepository<ProductTypeDetails, Integer> {
}
