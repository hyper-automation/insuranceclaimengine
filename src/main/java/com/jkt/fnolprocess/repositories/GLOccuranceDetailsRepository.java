package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.GLOccuranceDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Optional;

@Repository
public interface GLOccuranceDetailsRepository extends JpaRepository<GLOccuranceDetails, Integer> {
    Optional<GLOccuranceDetails> findTopByClaimIDOrderByModifiedOnDesc(String claimId);
}
