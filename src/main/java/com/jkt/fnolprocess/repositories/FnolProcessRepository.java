package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.ClaimDetails;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class FnolProcessRepository {

    @Autowired
    EntityManager entityManager;

    public List<String> getIncidentIDListByEmployee(Integer employeeId) {
        String query = "select cd.incidentID from claimDetails cd " +
                "left join policyDetails pd on cd.policyNumber = pd.policyNumber " +
                "left join employeeDetails ed on pd.employer = ed.employerID " +
                "where ed.employeeID = :employeeId";
        Session session = entityManager.unwrap(Session.class);

        return session.createNativeQuery(query)
                .setParameter("employeeId", employeeId)
                .list();
    }


    public List<String> getIncidentIDListByEmployer(Integer employerId) {
        String query = "select cd.incidentID from claimDetails cd " +
                "left join policyDetails pd on cd.policyNumber = pd.policyNumber " +
                "where pd.employer = :employerId";
        Session session = entityManager.unwrap(Session.class);

        return session.createNativeQuery(query)
                .setParameter("employerId", employerId)
                .list();
    }

    public List<String> getIncidentIDListByInsuredID(Integer insuredID) {
        String query = "select cd.incidentID from claimDetails cd " +
                "left join policyDetails pd on cd.policyNumber = pd.policyNumber " +
                "left join insuredDetails id on pd.employer = id.employerId " +
                "where id.insuredID = :insuredID";
        Session session = entityManager.unwrap(Session.class);

        return session.createNativeQuery(query)
                .setParameter("insuredID", insuredID)
                .list();
    }

}
