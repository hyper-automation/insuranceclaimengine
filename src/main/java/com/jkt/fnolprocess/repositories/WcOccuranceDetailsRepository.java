package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.WCOccuranceDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WcOccuranceDetailsRepository extends JpaRepository<WCOccuranceDetails,Integer> {
    Optional<WCOccuranceDetails> findTopByClaimIDOrderByModifiedOnDesc(String claimId);
}
