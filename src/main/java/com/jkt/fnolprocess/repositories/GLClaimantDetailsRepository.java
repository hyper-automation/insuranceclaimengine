package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.GLClaimantDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GLClaimantDetailsRepository extends JpaRepository<GLClaimantDetails , Integer> {
    Optional<GLClaimantDetails> findTopByClaimIDOrderByModifiedOnDesc(String claimId);
}
