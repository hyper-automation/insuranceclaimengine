package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.PropertyDetails;
import com.jkt.fnolprocess.entities.TreatmentDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PropertyDetailsRepository extends JpaRepository<PropertyDetails,Integer> {

    Optional<PropertyDetails> findByClaimID(String claimId);
}
