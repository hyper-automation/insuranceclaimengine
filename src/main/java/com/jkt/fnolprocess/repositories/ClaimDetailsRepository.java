package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.ClaimDetails;
import com.jkt.fnolprocess.entities.GLOccuranceDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClaimDetailsRepository extends JpaRepository<ClaimDetails, String> {

    List<ClaimDetails> findByIncidentID(String incidentID);
    List<ClaimDetails> findAllByPolicyNumber(String policyNumber);
    Optional<ClaimDetails> findTopByOrderByClaimIDDesc();

}
