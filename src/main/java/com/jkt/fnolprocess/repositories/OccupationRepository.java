package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.OccupationDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OccupationRepository extends JpaRepository<OccupationDetails, Integer> {
    Optional<OccupationDetails> findByOccupationDescription(String occupationDescription);
}
