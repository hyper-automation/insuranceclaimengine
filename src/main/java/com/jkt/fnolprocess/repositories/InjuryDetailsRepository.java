package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.InjuryDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Optional;

@Repository
public interface InjuryDetailsRepository extends JpaRepository<InjuryDetails,Integer> {

    List<InjuryDetails> findByClaimID(String claimId);
    Optional<InjuryDetails> findByClaimIDAndInjuryID(String claimId,Integer injuryId);

}
