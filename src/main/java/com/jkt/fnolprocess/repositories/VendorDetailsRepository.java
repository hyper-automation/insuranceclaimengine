package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.VendorDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VendorDetailsRepository extends JpaRepository<VendorDetails, Integer> {
}
