package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.PremisesTypeDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PremisesTypeRepository extends JpaRepository<PremisesTypeDetails , Integer> {
}
