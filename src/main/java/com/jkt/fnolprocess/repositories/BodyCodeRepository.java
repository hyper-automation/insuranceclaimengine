package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.BodyCodeDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Optional;

@Repository
public interface BodyCodeRepository extends JpaRepository<BodyCodeDetails , Integer> {
    List<BodyCodeDetails> findByBodyCodeCategoryID(Integer bodyCodeCategoryID);
}
