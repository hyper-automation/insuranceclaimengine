package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.ClaimDetails;
import com.jkt.fnolprocess.entities.IncidentDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IncidentDetailsRepository extends JpaRepository<IncidentDetails, Integer> {
    Optional<IncidentDetails> findByIncidentID(String incidentID);

    @Modifying
    @Query("update IncidentDetails id set id.subClaimType = :subClaimType where id.incidentID = :incidentID")
    int updateSubClaimType(@Param("subClaimType") Integer subClaimType, @Param("incidentID") String incidentID);
}
