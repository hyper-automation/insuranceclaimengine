package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.PolicyDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PolicyRepository extends JpaRepository<PolicyDetails, Integer> {
    Optional<PolicyDetails> findByEmployer(Integer employer);
}
