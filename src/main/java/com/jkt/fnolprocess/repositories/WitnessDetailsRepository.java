package com.jkt.fnolprocess.repositories;


import com.jkt.fnolprocess.entities.WitnessDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WitnessDetailsRepository extends JpaRepository<WitnessDetails,Integer> {
    List<WitnessDetails> findByClaimID(String claimID);


}
