package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.StateDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Optional;

@Repository
public interface StateRepository extends JpaRepository<StateDetails, Integer> {

    List<StateDetails> findBycountry(Integer country);

}
