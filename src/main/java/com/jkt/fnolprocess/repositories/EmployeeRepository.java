package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.EmployeeDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeDetails, Integer> {
    Optional<EmployeeDetails> findByInsuredID(Integer insuredID);
    List<EmployeeDetails> findByEmployerID(Integer employerID);

}
