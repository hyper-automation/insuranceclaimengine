package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.GLInjuredDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Optional;

@Repository
public interface GLInjuredDetailsRepository extends JpaRepository<GLInjuredDetails, Integer> {
    //Optional<GLInjuredDetails> findTopByOrderByModifiedOnAndClaimIDDesc(String claimId);
    Optional<GLInjuredDetails> findByClaimID(String claimId);

}
