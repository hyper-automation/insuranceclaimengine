package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.NcciCodeDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NcciCodeRepository extends JpaRepository<NcciCodeDetails, Integer> {

    Optional<NcciCodeDetails> findByNcciCode(String ncciCode);
}
