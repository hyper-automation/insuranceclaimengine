package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.InjuryCodeDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InjuryCodeRepository extends JpaRepository<InjuryCodeDetails, Integer> {

    List<InjuryCodeDetails> findByInjuryCodeCategoryID(Integer injuryCodeCategoryID);
}
