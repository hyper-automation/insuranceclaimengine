package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.ManufacturerDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManufacturerDetailsRepository extends JpaRepository<ManufacturerDetails,Integer> {
}
