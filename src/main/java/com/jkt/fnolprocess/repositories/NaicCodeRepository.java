package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.InjuryCodeDetails;
import com.jkt.fnolprocess.entities.NaicCodeDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NaicCodeRepository extends JpaRepository<NaicCodeDetails , Integer> {

    List<NaicCodeDetails> findByNaicCodeCategoryID(Integer naicCodeCategoryID);
}
