package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.CountryDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface CountryRepository extends JpaRepository<CountryDetails , Integer> {
    Optional<CountryDetails> findByCountryName(String countryName);
}
