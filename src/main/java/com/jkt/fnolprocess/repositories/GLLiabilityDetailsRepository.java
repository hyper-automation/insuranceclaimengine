package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.GLLiabilityDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GLLiabilityDetailsRepository extends JpaRepository<GLLiabilityDetails, Integer> {


    Optional<GLLiabilityDetails> findByClaimID(String claimId);
}
