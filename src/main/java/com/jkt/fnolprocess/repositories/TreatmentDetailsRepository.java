package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.TreatmentDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Optional;

@Repository
public interface TreatmentDetailsRepository extends JpaRepository<TreatmentDetails,Integer> {

    Optional<TreatmentDetails> findByInjuryIDAndTreatmentID(Integer injuryID, Integer treatmentID);
    List<TreatmentDetails> findByInjuryID(Integer injuryID);
}
