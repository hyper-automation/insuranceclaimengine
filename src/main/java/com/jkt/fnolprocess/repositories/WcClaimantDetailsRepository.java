package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.WCClaimantDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WcClaimantDetailsRepository extends JpaRepository<WCClaimantDetails,Integer> {
    Optional<WCClaimantDetails> findTopByClaimIDOrderByModifiedOnDesc(String claimId);


}
