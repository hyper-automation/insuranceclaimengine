package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.SubClaimType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubClaimTypeRepository extends JpaRepository<SubClaimType, Integer> {

}
