package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.SourceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SourceTypeRepository extends JpaRepository<SourceType, Integer> {
    Optional<SourceType> findBySourceType(String sourceType);
}
