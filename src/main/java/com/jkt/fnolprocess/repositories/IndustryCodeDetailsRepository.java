package com.jkt.fnolprocess.repositories;

import com.jkt.fnolprocess.entities.IndustryCodeDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IndustryCodeDetailsRepository extends JpaRepository<IndustryCodeDetails, Integer> {
}
